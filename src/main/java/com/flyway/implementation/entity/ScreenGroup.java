package com.flyway.implementation.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "screen_group", uniqueConstraints = @UniqueConstraint(
        columnNames = "screen_group_name", name = "unique_name_screengroup"
))
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ScreenGroup {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "screen_group_name")
    private String name;

    public void setName(String name) {
        this.name = name.trim().toUpperCase().replaceAll("\\s+", "");
    }
}
