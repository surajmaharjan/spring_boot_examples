package com.flyway.implementation.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "privilege", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"privilege_name"}, name = "unique_privilege_privilegename"),
})
@Builder
public class Privilege {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "privilege_name")
    private String name;

    public void setName(String name) {
        this.name = name.trim().toUpperCase().replaceAll("\\s+", "");
    }

    public Privilege(UUID id) {
        this.id = id;
    }
}
