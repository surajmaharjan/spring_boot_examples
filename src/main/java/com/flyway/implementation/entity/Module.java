package com.flyway.implementation.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name = "module", uniqueConstraints = @UniqueConstraint(
        columnNames = {"module_name"}, name = "unique_name_module"
))
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Module {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "module_name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "screen_id")
    private IndividualScreen individualScreen;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "module_privilege_table")
    private List<Privilege> privilegeList;

    public void setName(String name) {
        this.name = name.trim().toUpperCase().replaceAll("\\s+", "");
    }

    public Module(UUID id) {
        this.id = id;
    }
}
