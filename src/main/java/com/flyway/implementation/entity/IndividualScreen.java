package com.flyway.implementation.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "individual_screen", uniqueConstraints =
@UniqueConstraint(
        columnNames = {"screen_name"}, name = "unique_name_individualscreen"
))
public class IndividualScreen {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "screen_name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "screen_group_id")
    private ScreenGroup screenGroup;

    public void setName(String name) {
        this.name = name.trim().toUpperCase().replaceAll("\\s+", "");
    }
}
