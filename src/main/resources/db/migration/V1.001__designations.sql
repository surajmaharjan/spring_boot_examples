CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- auto-generated definition
create table if not exists designation
(
    id                 uuid         not null
        constraint designation_pkey
            primary key,
    created_date       timestamp,
    last_modified_date timestamp,
    created_by         uuid,
    last_modified_by   uuid,
    is_active          boolean,
    name               varchar(255) not null
        constraint unique_designation_name
            unique,
    name_n             varchar(255) not null
        constraint unique_designation_namen
            unique,
    shortname          varchar(255)
        constraint unique_designation_shortname
            unique
);

alter table designation
    owner to postgres;



alter table designation
    owner to postgres;

INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Auditor General', 'महालेखा परीक्षक', 'AG', true) ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Deputy Auditor General', 'उप महालेखा परीक्षक', 'DAG', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Assistant Auditor General', 'सहायक महालेखा परीक्षक', 'AAG', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Director', 'निर्देशक', 'DTR', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Audit Officer', 'अडिट अफिसर', 'AO', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Audit Inspector', 'अडिट इन्स्पेक्टर', 'AI', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Audit Superindent', 'अडिट सुपरइन्डेंट', 'AS', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'IT Officer', 'आइटि अधिकारी', 'ITO', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Auditee Chief', 'अडिटी  अधिकारी', 'AC', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Auditee Account Chief', 'अडिटी खाता अधिकारी', 'AAC', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Supervisor', 'सुपरभाइजर', 'SUPV', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Team Leader', 'टिम लिडर', 'TL', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Team Member', 'सदस्य', 'TM', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Office Head', 'कार्यालय प्रमुख', 'OFCHEAD', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Account Head', 'खाता प्रमुख', 'ACHEAD', true)ON conflict (shortname) do nothing;
INSERT INTO public.designation (id, name, name_n, shortname, is_active) VALUES (uuid_generate_v4(), 'Test', 'खाता', 'AAAA', true)ON conflict (shortname) do nothing;