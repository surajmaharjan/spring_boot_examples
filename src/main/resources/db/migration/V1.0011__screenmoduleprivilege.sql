-- insert screen group
-- this is creating an EXTENSION to generate random UUID
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- create table screen group if doesn't exists
create table if not exists screen_group
(
    id                uuid not null
        constraint screen_group_pkey
            primary key,
    screen_group_name varchar(255)
        constraint unique_name_screengroup
            unique
);

alter table screen_group
    owner to postgres;

 -- create screen groups
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'RISKBASEDANNUALAUDIT') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'MISREPORTING') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'USERMANAGEMENT') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'APPLICATIONMASTERSETTING') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'COMMUNICATIONMANAGEMENT') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'AUDITEXECUTION') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'COMMONGROUP') ON conflict (screen_group_name) do nothing;
INSERT INTO public.screen_group (id,screen_group_name) VALUES (uuid_generate_v4(),'MISREPORT') ON conflict (screen_group_name) do nothing;






-- create individual screen
-- create a table of the individual screen if not exists
-- auto-generated definition
create table if not exists individual_screen
(
    id              uuid not null
        constraint individual_screen_pkey
            primary key,
    screen_name     varchar(255)
        constraint unique_name_individualscreen
            unique,
    screen_group_id uuid
        constraint fkva3gqi1qu98tm8odxu3nd5co
            references screen_group
);

alter table individual_screen
    owner to postgres;






-- Function for creating individual screen starts
CREATE OR REPLACE FUNCTION individual_screen_function (sgn varchar , sn varchar , new_id uuid )
    RETURNS UUID AS $screen_grp_id$
        DECLARE screen_grp_id UUID;
    BEGIN
    SELECT  ID INTO screen_grp_id FROM screen_group where screen_group_name=sgn;
IF screen_grp_id IS NULL
THEN
    RETURN screen_grp_id;
ELSE
    INSERT INTO public.individual_screen (id,screen_name,screen_group_id) VALUES (new_id,sn,screen_grp_id) ON conflict (screen_name) do nothing;
END IF;

    RETURN screen_grp_id;
END;
$screen_grp_id$ LANGUAGE plpgsql;

-- Function for creating individual screen ends



-- APPLICATIONMASTERSETTING	OFFICEGROUPMAPPING
-- APPLICATIONMASTERSETTING	ZONALSETTING
-- APPLICATIONMASTERSETTING	OTHERSETTING
-- APPLICATIONMASTERSETTING	CHECKLISTSETTING
-- APPLICATIONMASTERSETTING	LANGUAGECONFIGURATION
-- APPLICATIONMASTERSETTING	DOCUMENTSETUP
-- APPLICATIONMASTERSETTING	AAPSETUP
-- APPLICATIONMASTERSETTING	AAPFORMCREATION
-- APPLICATIONMASTERSETTING	AUDITSETTING
-- APPLICATIONMASTERSETTING	RISKASSESSMENTSETTINGFORFA
-- APPLICATIONMASTERSETTING	SUBJECTMATTERFORCA
-- APPLICATIONMASTERSETTING	OAGORGANIZATIONSETTING
-- APPLICATIONMASTERSETTING	OFFICESETTING

select individual_screen_function('APPLICATIONMASTERSETTING','OFFICEGROUPMAPPING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','ZONALSETTING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','OTHERSETTING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','CHECKLISTSETTING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','LANGUAGECONFIGURATION',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','DOCUMENTSETUP',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','AAPSETUP',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','AAPFORMCREATION',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','AUDITSETTING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','RISKASSESSMENTSETTINGFORFA',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','SUBJECTMATTERFORCA',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','OAGORGANIZATIONSETTING',uuid_generate_v4());
select individual_screen_function('APPLICATIONMASTERSETTING','OFFICESETTING',uuid_generate_v4());

-- AUDITEXECUTION	REVISION
-- AUDITEXECUTION	AUDITEXECUTION

select individual_screen_function('AUDITEXECUTION','REVISION',uuid_generate_v4());
select individual_screen_function('AUDITEXECUTION','AUDITEXECUTION',uuid_generate_v4());

-- COMMONGROUP	DASHBOARD
-- COMMONGROUP	EMPLOYEEMANAGEMENT
-- COMMONGROUP	DOCUMENTMANAGEMENT

select individual_screen_function('COMMONGROUP','DASHBOARD',uuid_generate_v4());
select individual_screen_function('COMMONGROUP','EMPLOYEEMANAGEMENT',uuid_generate_v4());
select individual_screen_function('COMMONGROUP','DOCUMENTMANAGEMENT',uuid_generate_v4());

-- COMMUNICATIONMANAGEMENT	COMMUNICATIONMANAGEMENT

select individual_screen_function('COMMUNICATIONMANAGEMENT','COMMUNICATIONMANAGEMENT',uuid_generate_v4());

-- MISREPORTING	REPORTS
-- MISREPORTING	MISREPORTING
-- MISREPORTING	OFFICECHECKLISTSTATUS

select individual_screen_function('MISREPORTING','REPORTS',uuid_generate_v4());
select individual_screen_function('MISREPORTING','MISREPORTING',uuid_generate_v4());
select individual_screen_function('MISREPORTING','OFFICECHECKLISTSTATUS',uuid_generate_v4());

-- RISKBASEDANNUALAUDIT	ANNUALAUDITPLAN
-- RISKBASEDANNUALAUDIT	TEAMPLANNING
-- RISKBASEDANNUALAUDIT	OFFICEMANAGEMENT
-- RISKBASEDANNUALAUDIT	ENTITYMANAGEMENT
-- RISKBASEDANNUALAUDIT	ENTITYLEVELAUDITPLAN
-- RISKBASEDANNUALAUDIT	OFFICELEVELAUDITPLAN
-- RISKBASEDANNUALAUDIT	ANNUALAUDITPREREQUISITES
-- RISKBASEDANNUALAUDIT	OVERALLAUDITSTRATEGY
-- RISKBASEDANNUALAUDIT	AUDITPROGRAM
-- RISKBASEDANNUALAUDIT	APPROVEDAAPDOCUMENT

select individual_screen_function('RISKBASEDANNUALAUDIT','ANNUALAUDITPLAN',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','TEAMPLANNING',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','OFFICEMANAGEMENT',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','ENTITYMANAGEMENT',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','ENTITYLEVELAUDITPLAN',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','OFFICELEVELAUDITPLAN',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','ANNUALAUDITPREREQUISITES',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','OVERALLAUDITSTRATEGY',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','AUDITPROGRAM',uuid_generate_v4());
select individual_screen_function('RISKBASEDANNUALAUDIT','APPROVEDAAPDOCUMENT',uuid_generate_v4());

-- USERMANAGEMENT	USERSETTINGS
-- USERMANAGEMENT	ROLEMANAGEMENT
-- USERMANAGEMENT	SCREENMODULESETUP

select individual_screen_function('USERMANAGEMENT','USERSETTINGS',uuid_generate_v4());
select individual_screen_function('USERMANAGEMENT','ROLEMANAGEMENT',uuid_generate_v4());
select individual_screen_function('USERMANAGEMENT','SCREENMODULESETUP',uuid_generate_v4());







-- create modules

create table if not exists module
(
    id          uuid not null
        constraint module_pkey
            primary key,
    module_name varchar(255)
        constraint unique_name_module
            unique,
    screen_id   uuid
        constraint fkmhr1t5kdko83qjnusgwm7m52o
            references individual_screen
);

alter table module
    owner to postgres;





-- Function for creating module starts

CREATE OR REPLACE FUNCTION module_function (sc_name varchar , md_name varchar , new_id uuid )
    RETURNS UUID AS $screen_id$
        DECLARE screen_id UUID;
    BEGIN
    SELECT  ID INTO screen_id FROM individual_screen where screen_name=sc_name;
IF screen_id IS NULL
THEN
    RETURN screen_id;
ELSE
    INSERT INTO public.module (id,module_name,screen_id) VALUES (new_id,md_name,screen_id) ON conflict (module_name) do nothing ;
END IF;

    RETURN screen_id;
END;
$screen_id$ LANGUAGE plpgsql;

-- Function for creating module ends



-- data loading starts



-- AAPFORMCREATION	AAPFORMCREATION
-- AAPSETUP	AAPFIELDSETUP
-- AAPSETUP	AAPSETUP
select module_function('AAPFORMCREATION','AAPFORMCREATION',uuid_generate_v4());
select module_function('AAPSETUP','AAPFIELDSETUP',uuid_generate_v4());
select module_function('AAPSETUP','AAPSETUP',uuid_generate_v4());


-- ANNUALAUDITPLAN	TEAMCOMPOSITIONPA
-- ANNUALAUDITPLAN	ENTITYCHECKLISTMAPPING
-- ANNUALAUDITPLAN	AUDITCALENDAR
-- ANNUALAUDITPLAN	AUDITDEFINITION
-- ANNUALAUDITPLAN	MANDAYSCALCULATION
-- ANNUALAUDITPLAN	TEAMCOMPOSITIONOAP
-- ANNUALAUDITPLAN	TEAMCOMPOSITIONCA
-- ANNUALAUDITPLAN	AUDITPROGRAM
-- ANNUALAUDITPLAN	AUDITTEAMCOMPOSITION
-- ANNUALAUDITPLAN	AUDITANNEXURE
-- ANNUALAUDITPLAN	TEAMCOMPOSITIONMAP
-- ANNUALAUDITPLAN	SECTORCHECKLISTMAPPING
-- ANNUALAUDITPLAN	ENTITYGROUPCHECKLISTMAPPING
-- ANNUALAUDITPLAN	SUBJECTMATTERSELECTION
-- ANNUALAUDITPLAN	CUSTOMIZEDGROUPCHECKLISTMAPPING
-- ANNUALAUDITPLAN	RISKGRADINGFORFA
select module_function('ANNUALAUDITPLAN','TEAMCOMPOSITIONPA',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','ENTITYCHECKLISTMAPPING',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','AUDITCALENDAR',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','AUDITDEFINITION',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','MANDAYSCALCULATION',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','TEAMCOMPOSITIONOAP',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','TEAMCOMPOSITIONCA',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','AUDITPROGRAM',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','AUDITTEAMCOMPOSITION',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','AUDITANNEXURE',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','TEAMCOMPOSITIONMAP',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','SECTORCHECKLISTMAPPING',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','ENTITYGROUPCHECKLISTMAPPING',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','SUBJECTMATTERSELECTION',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','CUSTOMIZEDGROUPCHECKLISTMAPPING',uuid_generate_v4());
select module_function('ANNUALAUDITPLAN','RISKGRADINGFORFA',uuid_generate_v4());



-- ANNUALAUDITPREREQUISITES	AUDITDECLARATIONS
-- ANNUALAUDITPREREQUISITES	AUDITDECLARATIONSOVERVIEW
select module_function('ANNUALAUDITPREREQUISITES','AUDITDECLARATIONS',uuid_generate_v4());
select module_function('ANNUALAUDITPREREQUISITES','AUDITDECLARATIONSOVERVIEW',uuid_generate_v4());


-- APPROVEDAAPDOCUMENT	APPROVEDAAPDOCUMENT
select module_function('APPROVEDAAPDOCUMENT','APPROVEDAAPDOCUMENT',uuid_generate_v4());
-- AUDITEXECUTION	TESTOFCONTROLCREATE
-- AUDITEXECUTION	AECONCLUSION
-- AUDITEXECUTION	MINIMUMSUBSTANTIVEPROCEDURE
-- AUDITEXECUTION	ANALYTICALPROCEDURE
-- AUDITEXECUTION	TASKALLOCATION

-- AUDITEXECUTION	WRITTENREPRESENTATIONCONCLUSION
-- AUDITEXECUTION	OTHERPROCEDURES
-- AUDITEXECUTION	WRITTENREPRESENTATIONOFFICE
-- AUDITEXECUTION	WRITTENREPRESENTATIONENTITY
-- AUDITEXECUTION	SAMPLING

-- AUDITEXECUTION	AUTHORIZATIONLETTER
-- AUDITEXECUTION	TESTOFCONTROLCONCLUSION
-- AUDITEXECUTION	SUBSTANTIVEPROCEDURE
-- AUDITEXECUTION	TESTOFCONTROL
-- AUDITEXECUTION	EXITMEETINGMINUTECONCLUSION

-- AUDITEXECUTION	ENTRYMEETINGMINUTECONCLUSION
-- AUDITEXECUTION	EXITMEETINGMINUTECREATE
-- AUDITEXECUTION	EXITMEETINGMINUTE
-- AUDITEXECUTION	AUDITEXECUTIONLIST
-- AUDITEXECUTION	ENTRYMEETINGMINUTECREATE

-- AUDITEXECUTION	ENTRYMEETINGMINUTE
-- AUDITEXECUTION	PARREPORT
-- AUDITEXECUTION	AUDITEERESPONSE
select module_function('AUDITEXECUTION','TESTOFCONTROLCREATE',uuid_generate_v4());
select module_function('AUDITEXECUTION','AECONCLUSION',uuid_generate_v4());
select module_function('AUDITEXECUTION','MINIMUMSUBSTANTIVEPROCEDURE',uuid_generate_v4());
select module_function('AUDITEXECUTION','ANALYTICALPROCEDURE',uuid_generate_v4());
select module_function('AUDITEXECUTION','TASKALLOCATION',uuid_generate_v4());

select module_function('AUDITEXECUTION','WRITTENREPRESENTATIONCONCLUSION',uuid_generate_v4());
select module_function('AUDITEXECUTION','OTHERPROCEDURES',uuid_generate_v4());
select module_function('AUDITEXECUTION','WRITTENREPRESENTATIONOFFICE',uuid_generate_v4());
select module_function('AUDITEXECUTION','WRITTENREPRESENTATIONENTITY',uuid_generate_v4());
select module_function('AUDITEXECUTION','SAMPLING',uuid_generate_v4());

select module_function('AUDITEXECUTION','AUTHORIZATIONLETTER',uuid_generate_v4());
select module_function('AUDITEXECUTION','TESTOFCONTROLCONCLUSION',uuid_generate_v4());
select module_function('AUDITEXECUTION','SUBSTANTIVEPROCEDURE',uuid_generate_v4());
select module_function('AUDITEXECUTION','TESTOFCONTROL',uuid_generate_v4());
select module_function('AUDITEXECUTION','EXITMEETINGMINUTECONCLUSION',uuid_generate_v4());

select module_function('AUDITEXECUTION','ENTRYMEETINGMINUTECONCLUSION',uuid_generate_v4());
select module_function('AUDITEXECUTION','EXITMEETINGMINUTECREATE',uuid_generate_v4());
select module_function('AUDITEXECUTION','EXITMEETINGMINUTE',uuid_generate_v4());
select module_function('AUDITEXECUTION','AUDITEXECUTIONLIST',uuid_generate_v4());
select module_function('AUDITEXECUTION','ENTRYMEETINGMINUTECREATE',uuid_generate_v4());

select module_function('AUDITEXECUTION','ENTRYMEETINGMINUTE',uuid_generate_v4());
select module_function('AUDITEXECUTION','PARREPORT',uuid_generate_v4());
select module_function('AUDITEXECUTION','AUDITEERESPONSE',uuid_generate_v4());



-- AUDITPROGRAM	TEAMCOMPOSITION
-- AUDITPROGRAM	CONCLUSION_AUDITPROGRAM
-- AUDITPROGRAM	AUDITPLAN
-- AUDITPROGRAM	AUDITPROGRAMLIST
select module_function('AUDITPROGRAM','TEAMCOMPOSITION',uuid_generate_v4());
select module_function('AUDITPROGRAM','CONCLUSION_AUDITPROGRAM',uuid_generate_v4());
select module_function('AUDITPROGRAM','AUDITPLAN',uuid_generate_v4());
select module_function('AUDITPROGRAM','AUDITPROGRAMLIST',uuid_generate_v4());



-- AUDITSETTING	BERUJULAW
-- AUDITSETTING	AUDITWORKINGPAPER
-- AUDITSETTING	AUDITTYPE
-- AUDITSETTING	BERUJUSETTLEMENT
select module_function('AUDITSETTING','BERUJULAW',uuid_generate_v4());
select module_function('AUDITSETTING','AUDITWORKINGPAPER',uuid_generate_v4());
select module_function('AUDITSETTING','AUDITTYPE',uuid_generate_v4());
select module_function('AUDITSETTING','BERUJUSETTLEMENT',uuid_generate_v4());



-- CHECKLISTSETTING	AUDITAREAITEMSETUP
-- CHECKLISTSETTING	CHECKLISTOVERVIEW
-- CHECKLISTSETTING	CHECKLISTHEADSETUP
-- CHECKLISTSETTING	AUDITAREASETUP
-- CHECKLISTSETTING	CHECKLISTTYPESETUP
-- CHECKLISTSETTING	CHECKLISTGROUPSETUP

select module_function('CHECKLISTSETTING','AUDITAREAITEMSETUP',uuid_generate_v4());
select module_function('CHECKLISTSETTING','CHECKLISTOVERVIEW',uuid_generate_v4());
select module_function('CHECKLISTSETTING','CHECKLISTHEADSETUP',uuid_generate_v4());
select module_function('CHECKLISTSETTING','AUDITAREASETUP',uuid_generate_v4());
select module_function('CHECKLISTSETTING','CHECKLISTTYPESETUP',uuid_generate_v4());
select module_function('CHECKLISTSETTING','CHECKLISTGROUPSETUP',uuid_generate_v4());


-- COMMUNICATIONMANAGEMENT	MAILBOX
-- COMMUNICATIONMANAGEMENT	NOTICE
-- COMMUNICATIONMANAGEMENT	CHAT

select module_function('COMMUNICATIONMANAGEMENT','MAILBOX',uuid_generate_v4());
select module_function('COMMUNICATIONMANAGEMENT','NOTICE',uuid_generate_v4());
select module_function('COMMUNICATIONMANAGEMENT','CHAT',uuid_generate_v4());


-- DASHBOARD	SUPERVISORDASHBOARD
-- DASHBOARD	AAGDASHBOARD
-- DASHBOARD	CHANGEPASSWORD
-- DASHBOARD	SIDEBAR
-- DASHBOARD	HOMEPAGE
-- DASHBOARD	AUDITEEDASHBOARD
-- DASHBOARD	DAGDASHBOARD
select module_function('DASHBOARD','SUPERVISORDASHBOARD',uuid_generate_v4());
select module_function('DASHBOARD','AAGDASHBOARD',uuid_generate_v4());
select module_function('DASHBOARD','CHANGEPASSWORD',uuid_generate_v4());
select module_function('DASHBOARD','SIDEBAR',uuid_generate_v4());
select module_function('DASHBOARD','HOMEPAGE',uuid_generate_v4());
select module_function('DASHBOARD','AUDITEEDASHBOARD',uuid_generate_v4());
select module_function('DASHBOARD','DAGDASHBOARD',uuid_generate_v4());


-- DOCUMENTMANAGEMENT	DOCUMENTTAG
-- DOCUMENTMANAGEMENT	DOCUMENTMANAGEMENT
-- DOCUMENTMANAGEMENT	DOCUMENTTYPE
-- DOCUMENTMANAGEMENT	DOCUMENTGROUP
-- DOCUMENTMANAGEMENT	DOCUMENTVERSION
-- DOCUMENTMANAGEMENT	DOCUMENTUPLOAD
-- DOCUMENTMANAGEMENT	DOCUMENTDETAIL
select module_function('DOCUMENTMANAGEMENT','DOCUMENTTAG',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTMANAGEMENT',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTTYPE',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTGROUP',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTVERSION',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTUPLOAD',uuid_generate_v4());
select module_function('DOCUMENTMANAGEMENT','DOCUMENTDETAIL',uuid_generate_v4());






-- EMPLOYEEMANAGEMENT	EMPLOYMENTINFORMATION
-- EMPLOYEEMANAGEMENT	EDUCATIONINFORMATION
-- EMPLOYEEMANAGEMENT	TRAININGINFORMATION
-- EMPLOYEEMANAGEMENT	PROFESSIONALINFORMATION
-- EMPLOYEEMANAGEMENT	COMPETENCIESINFORMATION
-- EMPLOYEEMANAGEMENT	EMPLOYEECREATE
-- EMPLOYEEMANAGEMENT	EMPLOYEELIST
-- EMPLOYEEMANAGEMENT	BASICPROFILE
select module_function('EMPLOYEEMANAGEMENT','EMPLOYMENTINFORMATION',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','EDUCATIONINFORMATION',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','TRAININGINFORMATION',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','PROFESSIONALINFORMATION',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','COMPETENCIESINFORMATION',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','EMPLOYEECREATE',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','EMPLOYEELIST',uuid_generate_v4());
select module_function('EMPLOYEEMANAGEMENT','BASICPROFILE',uuid_generate_v4());



-- ENTITYLEVELAUDITPLAN	ATFINANCIALLEVEL-FA
-- ENTITYLEVELAUDITPLAN	EXPENDITUREREVENUE-FA
-- ENTITYLEVELAUDITPLAN	CONCLUSION-FA
-- ENTITYLEVELAUDITPLAN	MATERIALITY-FA
-- ENTITYLEVELAUDITPLAN	OVERALLAUDITSTRATEGYCA
-- ENTITYLEVELAUDITPLAN	ELPMASTER
-- ENTITYLEVELAUDITPLAN	ASSERTIONANDPERFORMANCE-FA
select module_function('ENTITYLEVELAUDITPLAN','ATFINANCIALLEVEL-FA',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','EXPENDITUREREVENUE-FA',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','CONCLUSION-FA',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','MATERIALITY-FA',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','OVERALLAUDITSTRATEGYCA',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','ELPMASTER',uuid_generate_v4());
select module_function('ENTITYLEVELAUDITPLAN','ASSERTIONANDPERFORMANCE-FA',uuid_generate_v4());



-- ENTITYMANAGEMENT	RISKOFMATERIALMISSTATEMENT
-- ENTITYMANAGEMENT	IDENTIFYINTERNALCONTROLS
-- ENTITYMANAGEMENT	ENTITYENVIRONMENT
select module_function('ENTITYMANAGEMENT','RISKOFMATERIALMISSTATEMENT',uuid_generate_v4());
select module_function('ENTITYMANAGEMENT','IDENTIFYINTERNALCONTROLS',uuid_generate_v4());
select module_function('ENTITYMANAGEMENT','ENTITYENVIRONMENT',uuid_generate_v4());



-- LANGUAGECONFIGURATION	JSONMODIFICATION
-- LANGUAGECONFIGURATION	LANGUAGEEDIT
select module_function('LANGUAGECONFIGURATION','JSONMODIFICATION',uuid_generate_v4());
select module_function('LANGUAGECONFIGURATION','LANGUAGEEDIT',uuid_generate_v4());


-- OAGORGANIZATIONSETTING	OAGSTRUCTURESETUP
-- OAGORGANIZATIONSETTING	OAGUNITLEVELSETUP
select module_function('OAGORGANIZATIONSETTING','OAGSTRUCTURESETUP',uuid_generate_v4());
select module_function('OAGORGANIZATIONSETTING','OAGUNITLEVELSETUP',uuid_generate_v4());


-- OFFICECHECKLISTSTATUS	OFFICECHECKLISTSTATUS
select module_function('OFFICECHECKLISTSTATUS','OFFICECHECKLISTSTATUS',uuid_generate_v4());

-- OFFICEGROUPMAPPING	CUSTOMIZEDGROUP
-- OFFICEGROUPMAPPING	SECTORALGROUPOFFICE
-- OFFICEGROUPMAPPING	SECTORALGROUPENTITYGROUP
-- OFFICEGROUPMAPPING	SECTORALGROUP
-- OFFICEGROUPMAPPING	ENTITYGROUPOFFICEMAPPING
-- OFFICEGROUPMAPPING	ENTITYGROUP
-- OFFICEGROUPMAPPING	CUSTOMIZEDGROUPMAPPING
select module_function('OFFICEGROUPMAPPING','CUSTOMIZEDGROUP',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','SECTORALGROUPOFFICE',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','SECTORALGROUPENTITYGROUP',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','SECTORALGROUP',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','ENTITYGROUPOFFICEMAPPING',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','ENTITYGROUP',uuid_generate_v4());
select module_function('OFFICEGROUPMAPPING','CUSTOMIZEDGROUPMAPPING',uuid_generate_v4());


-- OFFICELEVELAUDITPLAN	FINALCIALAUDITPLAN
-- OFFICELEVELAUDITPLAN	AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES
-- OFFICELEVELAUDITPLAN	AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL
-- OFFICELEVELAUDITPLAN	AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD
-- OFFICELEVELAUDITPLAN	AP2ENTITYLEVELAUDITPLAN
-- OFFICELEVELAUDITPLAN	AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY
-- OFFICELEVELAUDITPLAN	AP2CONCLUSION
-- OFFICELEVELAUDITPLAN	AP2TEAMPLANNING

select module_function('OFFICELEVELAUDITPLAN','FINALCIALAUDITPLAN',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2ENTITYLEVELAUDITPLAN',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2CONCLUSION',uuid_generate_v4());
select module_function('OFFICELEVELAUDITPLAN','AP2TEAMPLANNING',uuid_generate_v4());

-- OFFICEMANAGEMENT	RISKDATACOLLECTION
-- OFFICEMANAGEMENT	OFFICEINFORMATION
-- OFFICEMANAGEMENT	RISKREGISTER
-- OFFICEMANAGEMENT	OFFICEFINANCIALINFORMATION

select module_function('OFFICEMANAGEMENT','RISKDATACOLLECTION',uuid_generate_v4());
select module_function('OFFICEMANAGEMENT','OFFICEINFORMATION',uuid_generate_v4());
select module_function('OFFICEMANAGEMENT','RISKREGISTER',uuid_generate_v4());
select module_function('OFFICEMANAGEMENT','OFFICEFINANCIALINFORMATION',uuid_generate_v4());

-- OFFICESETTING	OFFICECATEGORYSETUP
-- OFFICESETTING	OFFICEMASTERSETUP

select module_function('OFFICESETTING','OFFICECATEGORYSETUP',uuid_generate_v4());
select module_function('OFFICESETTING','OFFICEMASTERSETUP',uuid_generate_v4());

-- OTHERSETTING	UNSETTLEMENTREASONSETUP
-- OTHERSETTING	SPECIALIZATIONSETUP
-- OTHERSETTING	DESIGNATIONSETUP
-- OTHERSETTING	TRAININGTYPESETUP
-- OTHERSETTING	PROFESSIONALQUALIFICATIONSETUP
-- OTHERSETTING	ACADEMICQUALIFICATIONSETUP

select module_function('OTHERSETTING','UNSETTLEMENTREASONSETUP',uuid_generate_v4());
select module_function('OTHERSETTING','SPECIALIZATIONSETUP',uuid_generate_v4());
select module_function('OTHERSETTING','DESIGNATIONSETUP',uuid_generate_v4());
select module_function('OTHERSETTING','TRAININGTYPESETUP',uuid_generate_v4());
select module_function('OTHERSETTING','PROFESSIONALQUALIFICATIONSETUP',uuid_generate_v4());
select module_function('OTHERSETTING','ACADEMICQUALIFICATIONSETUP',uuid_generate_v4());

-- OVERALLAUDITSTRATEGY	RISKASSESSMENT
-- OVERALLAUDITSTRATEGY	AUDITEEANDENVIRONMENT
-- OVERALLAUDITSTRATEGY	RELEVANTCONTROLENVIRONMENT
-- OVERALLAUDITSTRATEGY	DETERMININGMATERIALITY
-- OVERALLAUDITSTRATEGY	RISKOFFRAUD
-- OVERALLAUDITSTRATEGY	CONCLUSION
-- OVERALLAUDITSTRATEGY	OVERALLAUDITSTRATEGYLIST

select module_function('OVERALLAUDITSTRATEGY','RISKASSESSMENT',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','AUDITEEANDENVIRONMENT',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','RELEVANTCONTROLENVIRONMENT',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','DETERMININGMATERIALITY',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','RISKOFFRAUD',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','CONCLUSION',uuid_generate_v4());
select module_function('OVERALLAUDITSTRATEGY','OVERALLAUDITSTRATEGYLIST',uuid_generate_v4());


-- REPORTS	GENERAL
select module_function('REPORTS','GENERAL',uuid_generate_v4());


-- REVISION	UPDATECHECKLIST
-- REVISION	CHECKLISTFORALL
-- REVISION	CHECKLISTFORSINGLE
-- REVISION	BASICTODETAIL
-- REVISION	REVISION

select module_function('REVISION','UPDATECHECKLIST',uuid_generate_v4());
select module_function('REVISION','CHECKLISTFORALL',uuid_generate_v4());
select module_function('REVISION','CHECKLISTFORSINGLE',uuid_generate_v4());
select module_function('REVISION','BASICTODETAIL',uuid_generate_v4());
select module_function('REVISION','REVISION',uuid_generate_v4());

-- RISKASSESSMENTSETTINGFORFA	GRADINGCRITERIAFORFA
-- RISKASSESSMENTSETTINGFORFA	RISKSCOREPARAMENTERFORFA

select module_function('RISKASSESSMENTSETTINGFORFA','GRADINGCRITERIAFORFA',uuid_generate_v4());
select module_function('RISKASSESSMENTSETTINGFORFA','RISKSCOREPARAMENTERFORFA',uuid_generate_v4());

-- ROLEMANAGEMENT	USERPRIVILEGESETUP
-- ROLEMANAGEMENT	ROLESETUP
select module_function('ROLEMANAGEMENT','USERPRIVILEGESETUP',uuid_generate_v4());
select module_function('ROLEMANAGEMENT','ROLESETUP',uuid_generate_v4());

-- SCREENMODULESETUP	MODULESETUP
-- SCREENMODULESETUP	NOTIFICATIONGROUPSETUP
-- SCREENMODULESETUP	SCREENGROUPSETUP
-- SCREENMODULESETUP	PRIVILEGESETUP
-- SCREENMODULESETUP	SCREENSETUP

select module_function('SCREENMODULESETUP','MODULESETUP',uuid_generate_v4());
select module_function('SCREENMODULESETUP','NOTIFICATIONGROUPSETUP',uuid_generate_v4());
select module_function('SCREENMODULESETUP','SCREENGROUPSETUP',uuid_generate_v4());
select module_function('SCREENMODULESETUP','PRIVILEGESETUP',uuid_generate_v4());
select module_function('SCREENMODULESETUP','SCREENSETUP',uuid_generate_v4());

-- SUBJECTMATTERFORCA	SUBJECTMATTERIDENTIFICATION
-- SUBJECTMATTERFORCA	SUBJECTMATTERMASTER
-- SUBJECTMATTERFORCA	SUBJECTMATTERCONSOLIDATION

select module_function('SUBJECTMATTERFORCA','SUBJECTMATTERIDENTIFICATION',uuid_generate_v4());
select module_function('SUBJECTMATTERFORCA','SUBJECTMATTERMASTER',uuid_generate_v4());
select module_function('SUBJECTMATTERFORCA','SUBJECTMATTERCONSOLIDATION',uuid_generate_v4());


-- TEAMPLANNING	TEAMPLANNING
-- TEAMPLANNING	PERSONDAYSCALCULATION


select module_function('TEAMPLANNING','TEAMPLANNING',uuid_generate_v4());
select module_function('TEAMPLANNING','PERSONDAYSCALCULATION',uuid_generate_v4());
-- USERSETTINGS	USERSETUP
-- USERSETTINGS	EXTERNALUSER
-- USERSETTINGS	AUDITEEUSER
-- USERSETTINGS	OAGNUSER


select module_function('USERSETTINGS','USERSETUP',uuid_generate_v4());
select module_function('USERSETTINGS','EXTERNALUSER',uuid_generate_v4());
select module_function('USERSETTINGS','AUDITEEUSER',uuid_generate_v4());
select module_function('USERSETTINGS','OAGNUSER',uuid_generate_v4());


-- ZONALSETTING	DISTRICTSETUP
-- ZONALSETTING	LOCALLEVELSETUP
-- ZONALSETTING	GOVERNMENTSTRUCTURESETUP
-- ZONALSETTING	PROVINCESETUP
-- ZONALSETTING	COUNTRYSETUP

select module_function('ZONALSETTING','DISTRICTSETUP',uuid_generate_v4());
select module_function('ZONALSETTING','LOCALLEVELSETUP',uuid_generate_v4());
select module_function('ZONALSETTING','GOVERNMENTSTRUCTURESETUP',uuid_generate_v4());
select module_function('ZONALSETTING','PROVINCESETUP',uuid_generate_v4());
select module_function('ZONALSETTING','COUNTRYSETUP',uuid_generate_v4());



create table if not exists privilege
(
    id             uuid not null
        constraint privilege_pkey
            primary key,
    privilege_name varchar(255)
        constraint unique_privilege_privilegename
            unique
);

alter table privilege
    owner to postgres;



-- insert privileges

INSERT INTO privilege (id, privilege_name) VALUES ('d012c571-316d-409c-a9fc-1bde2105e2d3', 'CREATE') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('05b38ea8-96f3-4eeb-9437-eae607a60dde', 'READ') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('886a16f1-9181-4200-8a68-fe720103bc34', 'UPDATE') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('53aecea4-ea8d-4ccf-8d95-8cb3e3a64ce8', 'DELETE') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('d50596d6-86b8-4403-a27f-84a49d9d8a14', 'SEARCH') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('7582c050-ecf1-404f-9b53-dd6ac94764d6', 'CHANGESTATUS') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('985853c3-dc89-4b04-84d2-e2d4a3064268', 'APPROVE') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('aba5c28d-3d70-4458-95b7-1eec0c68c98d', 'RECOMMEND') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('59078a00-2c94-4e72-b8b1-b90a903ab6c0', 'REJECT') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('2f02bbe4-8a67-4013-9149-18d31e1ccb2a', 'SUBMIT') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('e8bd4fc6-a92d-4321-acf2-d251397b2939', 'REJECTDRAFT') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('63442232-0249-43ed-aee7-0465d854c322', 'REJECTVERIFIED') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('08640409-8846-4cc7-9324-d4a0da6e6f9f', 'REJECTRECOMMENDED') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('a16cb1f2-975d-4d5d-a044-556696ef3a54', 'VERIFY') ON conflict (privilege_name) do nothing ;
INSERT INTO privilege (id, privilege_name) VALUES ('5fe63824-e45c-4cd4-88e4-711de5770519', 'CONFIGURE') ON conflict (privilege_name) do nothing ;



-- this function returns the privilege id on parameter privilege name
CREATE OR REPLACE FUNCTION get_privilege_id (privilege_name_parameter varchar)
    RETURNS UUID AS $privilege_id$
DECLARE privilege_id UUID;
BEGIN
    SELECT  ID INTO privilege_id FROM privilege where privilege_name=privilege_name_parameter;
    RETURN privilege_id;
END;
$privilege_id$ LANGUAGE plpgsql;


-- this function returns the module id on parameter module name
CREATE OR REPLACE FUNCTION get_module_id (module_name_parameter varchar)
    RETURNS UUID AS $module_id$
DECLARE module_id UUID;
BEGIN
    SELECT  ID INTO module_id FROM module where module_name=module_name_parameter;
    RETURN module_id;
END;
$module_id$ LANGUAGE plpgsql;



create table if not exists module_privilege_table
(
    module_id         uuid not null
        constraint fk4fyy4cmuvntvvl3q19yj21ayr
            references module,
    privilege_list_id uuid not null
        constraint fko2u721r700h7j2xfbhgg4o3oy
            references privilege
);

alter table module_privilege_table
    owner to postgres;




-- this function inserts module and privilege
CREATE OR REPLACE FUNCTION create_module_privilege_table (module_id_parameter uuid , privilege_id_parameter uuid)
    RETURNS UUID AS $module_id_check$
DECLARE module_id_check UUID;
BEGIN
    select  module_id INTO module_id_check  from module_privilege_table where module_id=module_id_parameter and privilege_list_id=privilege_id_parameter;
    IF module_id_check IS NULL
    THEN
        INSERT INTO public.module_privilege_table (module_id, privilege_list_id) VALUES (module_id_parameter,privilege_id_parameter);
        RETURN module_id_check;
    END IF;
    RETURN module_id_check;
END;
$module_id_check$ LANGUAGE plpgsql;

-- start adding module privilege
-- AAGDASHBOARD	UPDATE
-- AAGDASHBOARD	DELETE
-- AAGDASHBOARD	SEARCH
-- AAGDASHBOARD	CREATE
-- AAGDASHBOARD	READ

select create_module_privilege_table(get_module_id('AAGDASHBOARD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AAGDASHBOARD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AAGDASHBOARD'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AAGDASHBOARD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AAGDASHBOARD'),get_privilege_id('READ'));


-- AAPFIELDSETUP	DELETE
-- AAPFIELDSETUP	CREATE
-- AAPFIELDSETUP	READ
-- AAPFIELDSETUP	UPDATE
-- AAPFIELDSETUP	SEARCH
select create_module_privilege_table(get_module_id('AAPFIELDSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AAPFIELDSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AAPFIELDSETUP'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AAPFIELDSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AAPFIELDSETUP'),get_privilege_id('READ'));


-- AAPFORMCREATION	READ
-- AAPFORMCREATION	SEARCH
-- AAPFORMCREATION	UPDATE
-- AAPFORMCREATION	DELETE
-- AAPFORMCREATION	CREATE

select create_module_privilege_table(get_module_id('AAPFORMCREATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AAPFORMCREATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AAPFORMCREATION'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AAPFORMCREATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AAPFORMCREATION'),get_privilege_id('READ'));

-- AAPSETUP	DELETE
-- AAPSETUP	CREATE
-- AAPSETUP	READ
-- AAPSETUP	SEARCH
-- AAPSETUP	UPDATE


-- ACADEMICQUALIFICATIONSETUP	CREATE
-- ACADEMICQUALIFICATIONSETUP	SEARCH
-- ACADEMICQUALIFICATIONSETUP	DELETE
-- ACADEMICQUALIFICATIONSETUP	UPDATE
-- ACADEMICQUALIFICATIONSETUP	READ
select create_module_privilege_table(get_module_id('ACADEMICQUALIFICATIONSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('ACADEMICQUALIFICATIONSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('ACADEMICQUALIFICATIONSETUP'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('ACADEMICQUALIFICATIONSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ACADEMICQUALIFICATIONSETUP'),get_privilege_id('READ'));


-- AECONCLUSION	UPDATE
-- AECONCLUSION	READ
-- AECONCLUSION	CREATE
select create_module_privilege_table(get_module_id('AECONCLUSION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AECONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AECONCLUSION'),get_privilege_id('UPDATE'));

-- ANALYTICALPROCEDURE	UPDATE
-- ANALYTICALPROCEDURE	CREATE
-- ANALYTICALPROCEDURE	READ
select create_module_privilege_table(get_module_id('ANALYTICALPROCEDURE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ANALYTICALPROCEDURE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ANALYTICALPROCEDURE'),get_privilege_id('UPDATE'));


-- AP2CONCLUSION	READ
-- AP2CONCLUSION	REJECT
-- AP2CONCLUSION	SUBMIT
-- AP2CONCLUSION	APPROVE
select create_module_privilege_table(get_module_id('AP2CONCLUSION'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('AP2CONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2CONCLUSION'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('AP2CONCLUSION'),get_privilege_id('APPROVE'));

-- AP2ENTITYLEVELAUDITPLAN	UPDATE
-- AP2ENTITYLEVELAUDITPLAN	CREATE
-- AP2ENTITYLEVELAUDITPLAN	APPROVE
-- AP2ENTITYLEVELAUDITPLAN	REJECT
-- AP2ENTITYLEVELAUDITPLAN	SUBMIT
-- AP2ENTITYLEVELAUDITPLAN	READ

select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('AP2ENTITYLEVELAUDITPLAN'),get_privilege_id('APPROVE'));

-- AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL	READ
-- AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL	UPDATE
-- AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL	CREATE
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANACCESSMENTOFCONTROL'),get_privilege_id('UPDATE'));

-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY	SEARCH
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY	UPDATE
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY	READ
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY	CREATE
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITY'),get_privilege_id('UPDATE'));

-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD	SEARCH
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD	UPDATE
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD	READ
-- AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD	CREATE

select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANPERFORMANCEMATERIALITYCOTABD'),get_privilege_id('UPDATE'));

-- AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES	SEARCH
-- AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES	DELETE
-- AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES	CREATE
-- AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES	READ
-- AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES	UPDATE
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AP2OFFICELEVELDETAILPLANTRANSACTIONANDACTIVITIES'),get_privilege_id('DELETE'));


-- AP2TEAMPLANNING	READ
-- AP2TEAMPLANNING	UPDATE
-- AP2TEAMPLANNING	CREATE
select create_module_privilege_table(get_module_id('AP2TEAMPLANNING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AP2TEAMPLANNING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AP2TEAMPLANNING'),get_privilege_id('UPDATE'));


-- APPROVEDAAPDOCUMENT	READ
select create_module_privilege_table(get_module_id('APPROVEDAAPDOCUMENT'),get_privilege_id('READ'));


-- ASSERTIONANDPERFORMANCE-FA	READ
-- ASSERTIONANDPERFORMANCE-FA	CREATE
-- ASSERTIONANDPERFORMANCE-FA	UPDATE
select create_module_privilege_table(get_module_id('ASSERTIONANDPERFORMANCE-FA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ASSERTIONANDPERFORMANCE-FA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ASSERTIONANDPERFORMANCE-FA'),get_privilege_id('UPDATE'));

-- ATFINANCIALLEVEL-FA	UPDATE
-- ATFINANCIALLEVEL-FA	READ
-- ATFINANCIALLEVEL-FA	CREATE
select create_module_privilege_table(get_module_id('ATFINANCIALLEVEL-FA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ATFINANCIALLEVEL-FA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ATFINANCIALLEVEL-FA'),get_privilege_id('UPDATE'));

-- AUDITANNEXURE	READ
-- AUDITANNEXURE	CREATE
-- AUDITANNEXURE	DELETE
-- AUDITANNEXURE	SEARCH
-- AUDITANNEXURE	UPDATE
select create_module_privilege_table(get_module_id('AUDITANNEXURE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITANNEXURE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITANNEXURE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITANNEXURE'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITANNEXURE'),get_privilege_id('SEARCH'));


-- AUDITAREAITEMSETUP	UPDATE
-- AUDITAREAITEMSETUP	DELETE
-- AUDITAREAITEMSETUP	CREATE
-- AUDITAREAITEMSETUP	SEARCH
-- AUDITAREAITEMSETUP	READ
select create_module_privilege_table(get_module_id('AUDITAREAITEMSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITAREAITEMSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITAREAITEMSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITAREAITEMSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITAREAITEMSETUP'),get_privilege_id('SEARCH'));

-- AUDITAREASETUP	READ
-- AUDITAREASETUP	SEARCH
-- AUDITAREASETUP	DELETE
-- AUDITAREASETUP	CREATE
-- AUDITAREASETUP	UPDATE
select create_module_privilege_table(get_module_id('AUDITAREASETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITAREASETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITAREASETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITAREASETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITAREASETUP'),get_privilege_id('SEARCH'));

-- AUDITCALENDAR	UPDATE
-- AUDITCALENDAR	CREATE
-- AUDITCALENDAR	READ
select create_module_privilege_table(get_module_id('AUDITCALENDAR'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITCALENDAR'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITCALENDAR'),get_privilege_id('UPDATE'));


-- AUDITDECLARATIONS	CREATE
-- AUDITDECLARATIONS	READ
-- AUDITDECLARATIONS	UPDATE
-- AUDITDECLARATIONS	SUBMIT
select create_module_privilege_table(get_module_id('AUDITDECLARATIONS'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITDECLARATIONS'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITDECLARATIONS'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITDECLARATIONS'),get_privilege_id('SUBMIT'));

-- AUDITDECLARATIONSOVERVIEW	READ
select create_module_privilege_table(get_module_id('AUDITDECLARATIONSOVERVIEW'),get_privilege_id('READ'));


-- AUDITDEFINITION	REJECTDRAFT
-- AUDITDEFINITION	RECOMMEND
-- AUDITDEFINITION	APPROVE
-- AUDITDEFINITION	UPDATE
-- AUDITDEFINITION	READ
-- AUDITDEFINITION	CREATE
-- AUDITDEFINITION	VERIFY
-- AUDITDEFINITION	REJECTRECOMMENDED
-- AUDITDEFINITION	REJECTVERIFIED
-- AUDITDEFINITION	SUBMIT
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('REJECTDRAFT'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('VERIFY'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('REJECTVERIFIED'));
select create_module_privilege_table(get_module_id('AUDITDEFINITION'),get_privilege_id('SUBMIT'));




-- AUDITEEANDENVIRONMENT	READ
-- AUDITEEANDENVIRONMENT	CREATE
-- AUDITEEANDENVIRONMENT	SEARCH
-- AUDITEEANDENVIRONMENT	DELETE
-- AUDITEEANDENVIRONMENT	UPDATE
select create_module_privilege_table(get_module_id('AUDITEEANDENVIRONMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITEEANDENVIRONMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITEEANDENVIRONMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITEEANDENVIRONMENT'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITEEANDENVIRONMENT'),get_privilege_id('SEARCH'));

-- AUDITEEDASHBOARD	UPDATE
-- AUDITEEDASHBOARD	READ
-- AUDITEEDASHBOARD	CREATE
-- AUDITEEDASHBOARD	SEARCH
-- AUDITEEDASHBOARD	DELETE
select create_module_privilege_table(get_module_id('AUDITEEDASHBOARD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITEEDASHBOARD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITEEDASHBOARD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITEEDASHBOARD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITEEDASHBOARD'),get_privilege_id('SEARCH'));

-- AUDITEERESPONSE	READ
-- AUDITEERESPONSE	DELETE
-- AUDITEERESPONSE	SEARCH
-- AUDITEERESPONSE	CREATE
-- AUDITEERESPONSE	UPDATE
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('SEARCH'));

-- AUDITEEUSER	UPDATE
-- AUDITEEUSER	READ
-- AUDITEEUSER	SEARCH
-- AUDITEEUSER	CHANGESTATUS
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('CHANGESTATUS'));
select create_module_privilege_table(get_module_id('AUDITEERESPONSE'),get_privilege_id('SEARCH'));

-- AUDITEXECUTIONLIST	CREATE
-- AUDITEXECUTIONLIST	READ
-- AUDITEXECUTIONLIST	UPDATE
select create_module_privilege_table(get_module_id('AUDITEXECUTIONLIST'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITEXECUTIONLIST'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITEXECUTIONLIST'),get_privilege_id('CREATE'));


-- AUDITPLAN	UPDATE
-- AUDITPLAN	DELETE
-- AUDITPLAN	SEARCH
-- AUDITPLAN	CREATE
-- AUDITPLAN	READ
select create_module_privilege_table(get_module_id('AUDITPLAN'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITPLAN'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITPLAN'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITPLAN'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITPLAN'),get_privilege_id('SEARCH'));

-- AUDITPROGRAM	READ
select create_module_privilege_table(get_module_id('AUDITPROGRAM'),get_privilege_id('READ'));


-- AUDITPROGRAMLIST	UPDATE
-- AUDITPROGRAMLIST	SEARCH
-- AUDITPROGRAMLIST	DELETE
-- AUDITPROGRAMLIST	READ
-- AUDITPROGRAMLIST	CREATE
select create_module_privilege_table(get_module_id('AUDITPROGRAMLIST'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITPROGRAMLIST'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITPROGRAMLIST'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITPROGRAMLIST'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITPROGRAMLIST'),get_privilege_id('SEARCH'));

-- AUDITTEAMCOMPOSITION	READ
select create_module_privilege_table(get_module_id('AUDITTEAMCOMPOSITION'),get_privilege_id('READ'));


-- AUDITTYPE	READ
-- AUDITTYPE	SEARCH
-- AUDITTYPE	DELETE
-- AUDITTYPE	UPDATE
-- AUDITTYPE	CREATE
select create_module_privilege_table(get_module_id('AUDITTYPE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITTYPE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITTYPE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITTYPE'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITTYPE'),get_privilege_id('SEARCH'));

-- AUDITWORKINGPAPER	SEARCH
-- AUDITWORKINGPAPER	DELETE
-- AUDITWORKINGPAPER	UPDATE
-- AUDITWORKINGPAPER	READ
-- AUDITWORKINGPAPER	CREATE
select create_module_privilege_table(get_module_id('AUDITWORKINGPAPER'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUDITWORKINGPAPER'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUDITWORKINGPAPER'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('AUDITWORKINGPAPER'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('AUDITWORKINGPAPER'),get_privilege_id('SEARCH'));

-- AUTHORIZATIONLETTER	CREATE
-- AUTHORIZATIONLETTER	UPDATE
-- AUTHORIZATIONLETTER	READ
select create_module_privilege_table(get_module_id('AUTHORIZATIONLETTER'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('AUTHORIZATIONLETTER'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('AUTHORIZATIONLETTER'),get_privilege_id('UPDATE'));

-- BASICPROFILE	READ
-- BASICPROFILE	CREATE
-- BASICPROFILE	UPDATE
select create_module_privilege_table(get_module_id('BASICPROFILE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('BASICPROFILE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('BASICPROFILE'),get_privilege_id('UPDATE'));

-- BASICTODETAIL	CREATE
select create_module_privilege_table(get_module_id('BASICTODETAIL'),get_privilege_id('CREATE'));


-- BERUJULAW	UPDATE
-- BERUJULAW	SEARCH
-- BERUJULAW	CREATE
-- BERUJULAW	READ
-- BERUJULAW	DELETE
select create_module_privilege_table(get_module_id('BERUJULAW'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('BERUJULAW'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('BERUJULAW'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('BERUJULAW'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('BERUJULAW'),get_privilege_id('SEARCH'));

-- BERUJUSETTLEMENT	SEARCH
-- BERUJUSETTLEMENT	UPDATE
-- BERUJUSETTLEMENT	DELETE
-- BERUJUSETTLEMENT	CREATE
-- BERUJUSETTLEMENT	READ
select create_module_privilege_table(get_module_id('BERUJUSETTLEMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('BERUJUSETTLEMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('BERUJUSETTLEMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('BERUJUSETTLEMENT'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('BERUJUSETTLEMENT'),get_privilege_id('SEARCH'));

-- CHANGEPASSWORD	READ
-- CHANGEPASSWORD	UPDATE
select create_module_privilege_table(get_module_id('CHANGEPASSWORD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHANGEPASSWORD'),get_privilege_id('UPDATE'));

-- CHAT	DELETE
-- CHAT	SEARCH
-- CHAT	READ
-- CHAT	UPDATE
-- CHAT	CREATE
select create_module_privilege_table(get_module_id('CHAT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHAT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHAT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CHAT'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CHAT'),get_privilege_id('SEARCH'));

-- CHECKLISTFORALL	READ
-- CHECKLISTFORALL	SUBMIT
-- CHECKLISTFORALL	UPDATE
-- CHECKLISTFORALL	CREATE
select create_module_privilege_table(get_module_id('CHECKLISTFORALL'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('CHECKLISTFORALL'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTFORALL'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHECKLISTFORALL'),get_privilege_id('UPDATE'));

-- CHECKLISTFORSINGLE	CREATE
-- CHECKLISTFORSINGLE	SUBMIT
-- CHECKLISTFORSINGLE	UPDATE
-- CHECKLISTFORSINGLE	READ
select create_module_privilege_table(get_module_id('CHECKLISTFORSINGLE'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('CHECKLISTFORSINGLE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTFORSINGLE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHECKLISTFORSINGLE'),get_privilege_id('UPDATE'));

-- CHECKLISTGROUPSETUP	UPDATE
-- CHECKLISTGROUPSETUP	READ
-- CHECKLISTGROUPSETUP	CREATE
-- CHECKLISTGROUPSETUP	DELETE
-- CHECKLISTGROUPSETUP	SEARCH
select create_module_privilege_table(get_module_id('CHECKLISTGROUPSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTGROUPSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHECKLISTGROUPSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CHECKLISTGROUPSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CHECKLISTGROUPSETUP'),get_privilege_id('SEARCH'));
-- CHECKLISTHEADSETUP	UPDATE
-- CHECKLISTHEADSETUP	CREATE
-- CHECKLISTHEADSETUP	SEARCH
-- CHECKLISTHEADSETUP	DELETE
-- CHECKLISTHEADSETUP	READ
select create_module_privilege_table(get_module_id('CHECKLISTHEADSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTHEADSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHECKLISTHEADSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CHECKLISTHEADSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CHECKLISTHEADSETUP'),get_privilege_id('SEARCH'));

-- CHECKLISTOVERVIEW	CREATE
-- CHECKLISTOVERVIEW	READ

select create_module_privilege_table(get_module_id('CHECKLISTOVERVIEW'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTOVERVIEW'),get_privilege_id('READ'));

-- CHECKLISTTYPESETUP	CREATE
-- CHECKLISTTYPESETUP	SEARCH
-- CHECKLISTTYPESETUP	DELETE
-- CHECKLISTTYPESETUP	UPDATE
-- CHECKLISTTYPESETUP	READ
select create_module_privilege_table(get_module_id('CHECKLISTTYPESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CHECKLISTTYPESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CHECKLISTTYPESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CHECKLISTTYPESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CHECKLISTTYPESETUP'),get_privilege_id('SEARCH'));

-- COMPETENCIESINFORMATION	CREATE
-- COMPETENCIESINFORMATION	UPDATE
-- COMPETENCIESINFORMATION	DELETE
-- COMPETENCIESINFORMATION	SEARCH
-- COMPETENCIESINFORMATION	READ
select create_module_privilege_table(get_module_id('COMPETENCIESINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('COMPETENCIESINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('COMPETENCIESINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('COMPETENCIESINFORMATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('COMPETENCIESINFORMATION'),get_privilege_id('SEARCH'));

-- CONCLUSION	REJECTVERIFIED
-- CONCLUSION	CONFIGURE
-- CONCLUSION	REJECTRECOMMENDED
-- CONCLUSION	VERIFY
-- CONCLUSION	REJECTDRAFT
-- CONCLUSION	SUBMIT
-- CONCLUSION	REJECT
-- CONCLUSION	APPROVE
-- CONCLUSION	RECOMMEND
-- CONCLUSION	CHANGESTATUS
-- CONCLUSION	SEARCH
-- CONCLUSION	DELETE
-- CONCLUSION	UPDATE
-- CONCLUSION	READ
-- CONCLUSION	CREATE
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('REJECTDRAFT'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('REJECTVERIFIED'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('CONFIGURE'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('CHANGESTATUS'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('VERIFY'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CONCLUSION'),get_privilege_id('CREATE'));

-- CONCLUSION-FA	REJECT
-- CONCLUSION-FA	CREATE
-- CONCLUSION-FA	READ
-- CONCLUSION-FA	UPDATE
-- CONCLUSION-FA	APPROVE
select create_module_privilege_table(get_module_id('CONCLUSION-FA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CONCLUSION-FA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CONCLUSION-FA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CONCLUSION-FA'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('CONCLUSION-FA'),get_privilege_id('REJECT'));

-- CONCLUSION_AUDITPROGRAM	VERIFY
-- CONCLUSION_AUDITPROGRAM	REJECTDRAFT
-- CONCLUSION_AUDITPROGRAM	SUBMIT
-- CONCLUSION_AUDITPROGRAM	CREATE
-- CONCLUSION_AUDITPROGRAM	READ
-- CONCLUSION_AUDITPROGRAM	UPDATE
-- CONCLUSION_AUDITPROGRAM	DELETE
-- CONCLUSION_AUDITPROGRAM	SEARCH
-- CONCLUSION_AUDITPROGRAM	CONFIGURE
-- CONCLUSION_AUDITPROGRAM	CHANGESTATUS
-- CONCLUSION_AUDITPROGRAM	REJECTVERIFIED
-- CONCLUSION_AUDITPROGRAM	APPROVE
-- CONCLUSION_AUDITPROGRAM	RECOMMEND
-- CONCLUSION_AUDITPROGRAM	REJECT
-- CONCLUSION_AUDITPROGRAM	REJECTRECOMMENDED
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('REJECTDRAFT'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('REJECTVERIFIED'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('CONFIGURE'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('CHANGESTATUS'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('VERIFY'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CONCLUSION_AUDITPROGRAM'),get_privilege_id('CREATE'));

-- COUNTRYSETUP	DELETE
-- COUNTRYSETUP	CREATE
-- COUNTRYSETUP	READ
-- COUNTRYSETUP	UPDATE
-- COUNTRYSETUP	SEARCH
select create_module_privilege_table(get_module_id('COUNTRYSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('COUNTRYSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('COUNTRYSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('COUNTRYSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('COUNTRYSETUP'),get_privilege_id('SEARCH'));

-- CUSTOMIZEDGROUP	CREATE
-- CUSTOMIZEDGROUP	SEARCH
-- CUSTOMIZEDGROUP	DELETE
-- CUSTOMIZEDGROUP	UPDATE
-- CUSTOMIZEDGROUP	READ
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUP'),get_privilege_id('SEARCH'));

-- CUSTOMIZEDGROUPCHECKLISTMAPPING	CREATE
-- CUSTOMIZEDGROUPCHECKLISTMAPPING	READ
-- CUSTOMIZEDGROUPCHECKLISTMAPPING	UPDATE
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPCHECKLISTMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPCHECKLISTMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPCHECKLISTMAPPING'),get_privilege_id('UPDATE'));

-- CUSTOMIZEDGROUPMAPPING	CREATE
-- CUSTOMIZEDGROUPMAPPING	READ
-- CUSTOMIZEDGROUPMAPPING	UPDATE
-- CUSTOMIZEDGROUPMAPPING	DELETE
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPMAPPING'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('CUSTOMIZEDGROUPMAPPING'),get_privilege_id('DELETE'));


-- DAGDASHBOARD	READ
-- DAGDASHBOARD	DELETE
-- DAGDASHBOARD	SEARCH
-- DAGDASHBOARD	CREATE
-- DAGDASHBOARD	UPDATE
select create_module_privilege_table(get_module_id('DAGDASHBOARD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DAGDASHBOARD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DAGDASHBOARD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DAGDASHBOARD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DAGDASHBOARD'),get_privilege_id('SEARCH'));

-- DESIGNATIONSETUP	CREATE
-- DESIGNATIONSETUP	UPDATE
-- DESIGNATIONSETUP	SEARCH
-- DESIGNATIONSETUP	DELETE
-- DESIGNATIONSETUP	READ
select create_module_privilege_table(get_module_id('DESIGNATIONSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DESIGNATIONSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DESIGNATIONSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DESIGNATIONSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DESIGNATIONSETUP'),get_privilege_id('SEARCH'));

-- DETERMININGMATERIALITY	DELETE
-- DETERMININGMATERIALITY	READ
-- DETERMININGMATERIALITY	CREATE
-- DETERMININGMATERIALITY	SEARCH
-- DETERMININGMATERIALITY	UPDATE
select create_module_privilege_table(get_module_id('DETERMININGMATERIALITY'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DETERMININGMATERIALITY'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DETERMININGMATERIALITY'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DETERMININGMATERIALITY'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DETERMININGMATERIALITY'),get_privilege_id('SEARCH'));

-- DISTRICTSETUP	READ
-- DISTRICTSETUP	CREATE
-- DISTRICTSETUP	SEARCH
-- DISTRICTSETUP	DELETE
-- DISTRICTSETUP	UPDATE

select create_module_privilege_table(get_module_id('DISTRICTSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DISTRICTSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DISTRICTSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DISTRICTSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DISTRICTSETUP'),get_privilege_id('SEARCH'));
-- DOCUMENTDETAIL	READ
-- DOCUMENTDETAIL	UPDATE
-- DOCUMENTDETAIL	DELETE
-- DOCUMENTDETAIL	SEARCH
-- DOCUMENTDETAIL	APPROVE
-- DOCUMENTDETAIL	REJECT
-- DOCUMENTDETAIL	CREATE
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('DOCUMENTDETAIL'),get_privilege_id('APPROVE'));



-- DOCUMENTGROUP	UPDATE
-- DOCUMENTGROUP	READ
-- DOCUMENTGROUP	SEARCH
-- DOCUMENTGROUP	CREATE
-- DOCUMENTGROUP	DELETE
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('SEARCH'));



-- DOCUMENTMANAGEMENT	CHANGESTATUS
-- DOCUMENTMANAGEMENT	DELETE
-- DOCUMENTMANAGEMENT	REJECT
-- DOCUMENTMANAGEMENT	RECOMMEND
-- DOCUMENTMANAGEMENT	APPROVE
-- DOCUMENTMANAGEMENT	UPDATE
-- DOCUMENTMANAGEMENT	CONFIGURE
-- DOCUMENTMANAGEMENT	SEARCH
-- DOCUMENTMANAGEMENT	CREATE
-- DOCUMENTMANAGEMENT	READ
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('CONFIGURE'));
select create_module_privilege_table(get_module_id('DOCUMENTGROUP'),get_privilege_id('CHANGESTATUS'));



-- DOCUMENTTAG	READ
-- DOCUMENTTAG	UPDATE
-- DOCUMENTTAG	DELETE
-- DOCUMENTTAG	SEARCH
-- DOCUMENTTAG	CREATE
select create_module_privilege_table(get_module_id('DOCUMENTTAG'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTTAG'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTTAG'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTTAG'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DOCUMENTTAG'),get_privilege_id('SEARCH'));



-- DOCUMENTTYPE	CREATE
-- DOCUMENTTYPE	READ
-- DOCUMENTTYPE	DELETE
-- DOCUMENTTYPE	UPDATE

select create_module_privilege_table(get_module_id('DOCUMENTTYPE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTTYPE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTTYPE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTTYPE'),get_privilege_id('DELETE'));


-- DOCUMENTUPLOAD	CREATE
-- DOCUMENTUPLOAD	DELETE
-- DOCUMENTUPLOAD	UPDATE
-- DOCUMENTUPLOAD	READ
-- DOCUMENTUPLOAD	SEARCH
select create_module_privilege_table(get_module_id('DOCUMENTUPLOAD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTUPLOAD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTUPLOAD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTUPLOAD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('DOCUMENTUPLOAD'),get_privilege_id('SEARCH'));


-- DOCUMENTVERSION	READ
-- DOCUMENTVERSION	CREATE
-- DOCUMENTVERSION	UPDATE
-- DOCUMENTVERSION	DELETE
select create_module_privilege_table(get_module_id('DOCUMENTVERSION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('DOCUMENTVERSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('DOCUMENTVERSION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('DOCUMENTVERSION'),get_privilege_id('DELETE'));


-- EDUCATIONINFORMATION	CREATE
-- EDUCATIONINFORMATION	SEARCH
-- EDUCATIONINFORMATION	DELETE
-- EDUCATIONINFORMATION	UPDATE
-- EDUCATIONINFORMATION	READ
select create_module_privilege_table(get_module_id('EDUCATIONINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EDUCATIONINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EDUCATIONINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('EDUCATIONINFORMATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('EDUCATIONINFORMATION'),get_privilege_id('SEARCH'));


-- ELPMASTER	APPROVE
-- ELPMASTER	REJECTRECOMMENDED
-- ELPMASTER	REJECTDRAFT
-- ELPMASTER	SUBMIT
-- ELPMASTER	RECOMMEND

select create_module_privilege_table(get_module_id('ELPMASTER'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('ELPMASTER'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('ELPMASTER'),get_privilege_id('REJECTDRAFT'));
select create_module_privilege_table(get_module_id('ELPMASTER'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('ELPMASTER'),get_privilege_id('RECOMMEND'));


-- EMPLOYEECREATE	UPDATE
-- EMPLOYEECREATE	DELETE
-- EMPLOYEECREATE	SEARCH
-- EMPLOYEECREATE	CREATE
-- EMPLOYEECREATE	READ
select create_module_privilege_table(get_module_id('EMPLOYEECREATE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EMPLOYEECREATE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EMPLOYEECREATE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('EMPLOYEECREATE'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('EMPLOYEECREATE'),get_privilege_id('SEARCH'));


-- EMPLOYEELIST	UPDATE
-- EMPLOYEELIST	READ
-- EMPLOYEELIST	CREATE
-- EMPLOYEELIST	SEARCH
-- EMPLOYEELIST	DELETE
select create_module_privilege_table(get_module_id('EMPLOYEELIST'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EMPLOYEELIST'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EMPLOYEELIST'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('EMPLOYEELIST'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('EMPLOYEELIST'),get_privilege_id('SEARCH'));


-- EMPLOYMENTINFORMATION	READ
-- EMPLOYMENTINFORMATION	SEARCH
-- EMPLOYMENTINFORMATION	DELETE
-- EMPLOYMENTINFORMATION	UPDATE
-- EMPLOYMENTINFORMATION	CREATE

select create_module_privilege_table(get_module_id('EMPLOYMENTINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EMPLOYMENTINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EMPLOYMENTINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('EMPLOYMENTINFORMATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('EMPLOYMENTINFORMATION'),get_privilege_id('SEARCH'));

-- ENTITYCHECKLISTMAPPING	CREATE
-- ENTITYCHECKLISTMAPPING	READ
-- ENTITYCHECKLISTMAPPING	UPDATE
select create_module_privilege_table(get_module_id('ENTITYCHECKLISTMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTITYCHECKLISTMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTITYCHECKLISTMAPPING'),get_privilege_id('UPDATE'));



-- ENTITYENVIRONMENT	UPDATE
-- ENTITYENVIRONMENT	READ
-- ENTITYENVIRONMENT	DELETE
-- ENTITYENVIRONMENT	CREATE
select create_module_privilege_table(get_module_id('ENTITYENVIRONMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTITYENVIRONMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTITYENVIRONMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('ENTITYENVIRONMENT'),get_privilege_id('DELETE'));


-- ENTITYGROUP	READ
-- ENTITYGROUP	UPDATE
-- ENTITYGROUP	DELETE
-- ENTITYGROUP	SEARCH
-- ENTITYGROUP	CREATE
select create_module_privilege_table(get_module_id('ENTITYGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTITYGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTITYGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('ENTITYGROUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('ENTITYGROUP'),get_privilege_id('SEARCH'));



-- ENTITYGROUPCHECKLISTMAPPING	CREATE
-- ENTITYGROUPCHECKLISTMAPPING	READ
-- ENTITYGROUPCHECKLISTMAPPING	UPDATE
select create_module_privilege_table(get_module_id('ENTITYGROUPCHECKLISTMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTITYGROUPCHECKLISTMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTITYGROUPCHECKLISTMAPPING'),get_privilege_id('UPDATE'));



-- ENTITYGROUPOFFICEMAPPING	CREATE
-- ENTITYGROUPOFFICEMAPPING	READ
-- ENTITYGROUPOFFICEMAPPING	DELETE
-- ENTITYGROUPOFFICEMAPPING	UPDATE
select create_module_privilege_table(get_module_id('ENTITYGROUPOFFICEMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTITYGROUPOFFICEMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTITYGROUPOFFICEMAPPING'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('ENTITYGROUPOFFICEMAPPING'),get_privilege_id('DELETE'));


-- ENTRYMEETINGMINUTE	UPDATE
-- ENTRYMEETINGMINUTE	CREATE
-- ENTRYMEETINGMINUTE	READ
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTE'),get_privilege_id('UPDATE'));



-- ENTRYMEETINGMINUTECONCLUSION	CREATE
-- ENTRYMEETINGMINUTECONCLUSION	UPDATE
-- ENTRYMEETINGMINUTECONCLUSION	READ
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECONCLUSION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECONCLUSION'),get_privilege_id('UPDATE'));


-- ENTRYMEETINGMINUTECREATE	UPDATE
-- ENTRYMEETINGMINUTECREATE	CREATE
-- ENTRYMEETINGMINUTECREATE	READ
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECREATE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECREATE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ENTRYMEETINGMINUTECREATE'),get_privilege_id('UPDATE'));



-- EXITMEETINGMINUTE	CREATE
-- EXITMEETINGMINUTE	UPDATE
-- EXITMEETINGMINUTE	READ

select create_module_privilege_table(get_module_id('EXITMEETINGMINUTE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTE'),get_privilege_id('UPDATE'));


-- EXITMEETINGMINUTECONCLUSION	READ
-- EXITMEETINGMINUTECONCLUSION	CREATE
-- EXITMEETINGMINUTECONCLUSION	UPDATE

select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECONCLUSION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECONCLUSION'),get_privilege_id('UPDATE'));


-- EXITMEETINGMINUTECREATE	CREATE
-- EXITMEETINGMINUTECREATE	UPDATE
-- EXITMEETINGMINUTECREATE	READ

select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECREATE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECREATE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EXITMEETINGMINUTECREATE'),get_privilege_id('UPDATE'));


-- EXPENDITUREREVENUE-FA	UPDATE
-- EXPENDITUREREVENUE-FA	CREATE
-- EXPENDITUREREVENUE-FA	READ

select create_module_privilege_table(get_module_id('EXPENDITUREREVENUE-FA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('EXPENDITUREREVENUE-FA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EXPENDITUREREVENUE-FA'),get_privilege_id('UPDATE'));



-- EXTERNALUSER	UPDATE
-- EXTERNALUSER	CHANGESTATUS
-- EXTERNALUSER	SEARCH
-- EXTERNALUSER	READ
select create_module_privilege_table(get_module_id('EXTERNALUSER'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('EXTERNALUSER'),get_privilege_id('CHANGESTATUS'));
select create_module_privilege_table(get_module_id('EXTERNALUSER'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('EXTERNALUSER'),get_privilege_id('UPDATE'));




-- FINALCIALAUDITPLAN	SEARCH
-- FINALCIALAUDITPLAN	CREATE
-- FINALCIALAUDITPLAN	READ
-- FINALCIALAUDITPLAN	UPDATE
-- FINALCIALAUDITPLAN	DELETE
-- FINALCIALAUDITPLAN	APPROVE
-- FINALCIALAUDITPLAN	REJECT
-- FINALCIALAUDITPLAN	SUBMIT
-- FINALCIALAUDITPLAN	REJECTVERIFIED
-- FINALCIALAUDITPLAN	RECOMMEND
-- FINALCIALAUDITPLAN	REJECTDRAFT
-- FINALCIALAUDITPLAN	REJECTRECOMMENDED
-- FINALCIALAUDITPLAN	VERIFY
-- FINALCIALAUDITPLAN	CHANGESTATUS
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('REJECTDRAFT'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('REJECTVERIFIED'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('CHANGESTATUS'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('VERIFY'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('SEARCH'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('FINALCIALAUDITPLAN'),get_privilege_id('CREATE'));



-- GENERAL	READ
-- GENERAL	DELETE
-- GENERAL	UPDATE
select create_module_privilege_table(get_module_id('GENERAL'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('GENERAL'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('GENERAL'),get_privilege_id('DELETE'));



-- GOVERNMENTSTRUCTURESETUP	READ
-- GOVERNMENTSTRUCTURESETUP	CREATE
-- GOVERNMENTSTRUCTURESETUP	SEARCH
-- GOVERNMENTSTRUCTURESETUP	DELETE
-- GOVERNMENTSTRUCTURESETUP	UPDATE
select create_module_privilege_table(get_module_id('GOVERNMENTSTRUCTURESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('GOVERNMENTSTRUCTURESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('GOVERNMENTSTRUCTURESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('GOVERNMENTSTRUCTURESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('GOVERNMENTSTRUCTURESETUP'),get_privilege_id('SEARCH'));




-- GRADINGCRITERIAFORFA	READ
-- GRADINGCRITERIAFORFA	SEARCH
-- GRADINGCRITERIAFORFA	DELETE
-- GRADINGCRITERIAFORFA	CREATE
-- GRADINGCRITERIAFORFA	UPDATE

select create_module_privilege_table(get_module_id('GRADINGCRITERIAFORFA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('GRADINGCRITERIAFORFA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('GRADINGCRITERIAFORFA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('GRADINGCRITERIAFORFA'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('GRADINGCRITERIAFORFA'),get_privilege_id('SEARCH'));


-- HOMEPAGE	READ
select create_module_privilege_table(get_module_id('HOMEPAGE'),get_privilege_id('READ'));



-- IDENTIFYINTERNALCONTROLS	READ
-- IDENTIFYINTERNALCONTROLS	DELETE
-- IDENTIFYINTERNALCONTROLS	CREATE
-- IDENTIFYINTERNALCONTROLS	UPDATE

select create_module_privilege_table(get_module_id('IDENTIFYINTERNALCONTROLS'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('IDENTIFYINTERNALCONTROLS'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('IDENTIFYINTERNALCONTROLS'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('IDENTIFYINTERNALCONTROLS'),get_privilege_id('DELETE'));


-- JSONMODIFICATION	READ
-- JSONMODIFICATION	UPDATE
-- JSONMODIFICATION	CREATE
select create_module_privilege_table(get_module_id('JSONMODIFICATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('JSONMODIFICATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('JSONMODIFICATION'),get_privilege_id('UPDATE'));




-- LANGUAGEEDIT	READ
-- LANGUAGEEDIT	UPDATE
-- LANGUAGEEDIT	SEARCH
select create_module_privilege_table(get_module_id('LANGUAGEEDIT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('LANGUAGEEDIT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('LANGUAGEEDIT'),get_privilege_id('SEARCH'));




-- LOCALLEVELSETUP	UPDATE
-- LOCALLEVELSETUP	CREATE
-- LOCALLEVELSETUP	READ
-- LOCALLEVELSETUP	DELETE
-- LOCALLEVELSETUP	SEARCH
select create_module_privilege_table(get_module_id('LOCALLEVELSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('LOCALLEVELSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('LOCALLEVELSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('LOCALLEVELSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('LOCALLEVELSETUP'),get_privilege_id('SEARCH'));



-- MAILBOX	SEARCH
-- MAILBOX	DELETE
-- MAILBOX	UPDATE
-- MAILBOX	READ
-- MAILBOX	CREATE
select create_module_privilege_table(get_module_id('MAILBOX'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('MAILBOX'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('MAILBOX'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('MAILBOX'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('MAILBOX'),get_privilege_id('SEARCH'));



-- MANDAYSCALCULATION	CREATE
-- MANDAYSCALCULATION	READ
-- MANDAYSCALCULATION	UPDATE
select create_module_privilege_table(get_module_id('MANDAYSCALCULATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('MANDAYSCALCULATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('MANDAYSCALCULATION'),get_privilege_id('UPDATE'));



-- MATERIALITY-FA	UPDATE
-- MATERIALITY-FA	SEARCH
-- MATERIALITY-FA	DELETE
-- MATERIALITY-FA	CREATE
-- MATERIALITY-FA	READ

select create_module_privilege_table(get_module_id('MATERIALITY-FA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('MATERIALITY-FA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('MATERIALITY-FA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('MATERIALITY-FA'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('MATERIALITY-FA'),get_privilege_id('SEARCH'));


-- MINIMUMSUBSTANTIVEPROCEDURE	READ
-- MINIMUMSUBSTANTIVEPROCEDURE	UPDATE
-- MINIMUMSUBSTANTIVEPROCEDURE	CREATE

select create_module_privilege_table(get_module_id('MINIMUMSUBSTANTIVEPROCEDURE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('MINIMUMSUBSTANTIVEPROCEDURE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('MINIMUMSUBSTANTIVEPROCEDURE'),get_privilege_id('UPDATE'));



-- MODULESETUP	READ
-- MODULESETUP	CREATE
-- MODULESETUP	UPDATE
-- MODULESETUP	SEARCH
select create_module_privilege_table(get_module_id('MODULESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('MODULESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('MODULESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('MODULESETUP'),get_privilege_id('SEARCH'));



-- NOTICE	DELETE
-- NOTICE	SEARCH
-- NOTICE	UPDATE
-- NOTICE	READ
-- NOTICE	CREATE

select create_module_privilege_table(get_module_id('NOTICE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('NOTICE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('NOTICE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('NOTICE'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('NOTICE'),get_privilege_id('SEARCH'));


-- NOTIFICATIONGROUPSETUP	READ
-- NOTIFICATIONGROUPSETUP	DELETE
-- NOTIFICATIONGROUPSETUP	UPDATE
-- NOTIFICATIONGROUPSETUP	CREATE
select create_module_privilege_table(get_module_id('NOTIFICATIONGROUPSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('NOTIFICATIONGROUPSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('NOTIFICATIONGROUPSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('NOTIFICATIONGROUPSETUP'),get_privilege_id('DELETE'));



-- OAGNUSER	SEARCH
-- OAGNUSER	CHANGESTATUS
-- OAGNUSER	UPDATE
-- OAGNUSER	READ

select create_module_privilege_table(get_module_id('OAGNUSER'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OAGNUSER'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OAGNUSER'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OAGNUSER'),get_privilege_id('SEARCH'));




-- OAGSTRUCTURESETUP	CREATE
-- OAGSTRUCTURESETUP	SEARCH
-- OAGSTRUCTURESETUP	DELETE
-- OAGSTRUCTURESETUP	UPDATE
-- OAGSTRUCTURESETUP	READ
select create_module_privilege_table(get_module_id('OAGSTRUCTURESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OAGSTRUCTURESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OAGSTRUCTURESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OAGSTRUCTURESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OAGSTRUCTURESETUP'),get_privilege_id('SEARCH'));

-- OAGUNITLEVELSETUP	SEARCH
-- OAGUNITLEVELSETUP	DELETE
-- OAGUNITLEVELSETUP	CREATE
-- OAGUNITLEVELSETUP	READ
-- OAGUNITLEVELSETUP	UPDATE

select create_module_privilege_table(get_module_id('OAGUNITLEVELSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OAGUNITLEVELSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OAGUNITLEVELSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OAGUNITLEVELSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OAGUNITLEVELSETUP'),get_privilege_id('SEARCH'));



-- OFFICECATEGORYSETUP	CREATE
-- OFFICECATEGORYSETUP	READ
-- OFFICECATEGORYSETUP	SEARCH
-- OFFICECATEGORYSETUP	UPDATE
-- OFFICECATEGORYSETUP	DELETE
select create_module_privilege_table(get_module_id('OFFICECATEGORYSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OFFICECATEGORYSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OFFICECATEGORYSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OFFICECATEGORYSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OFFICECATEGORYSETUP'),get_privilege_id('SEARCH'));



-- OFFICECHECKLISTSTATUS	REJECTRECOMMENDED
-- OFFICECHECKLISTSTATUS	RECOMMEND
-- OFFICECHECKLISTSTATUS	APPROVE
-- OFFICECHECKLISTSTATUS	REJECTDRAFT

select create_module_privilege_table(get_module_id('OFFICECHECKLISTSTATUS'),get_privilege_id('REJECTRECOMMENDED'));
select create_module_privilege_table(get_module_id('OFFICECHECKLISTSTATUS'),get_privilege_id('RECOMMEND'));
select create_module_privilege_table(get_module_id('OFFICECHECKLISTSTATUS'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('OFFICECHECKLISTSTATUS'),get_privilege_id('REJECTDRAFT'));





-- OFFICEINFORMATION	CREATE
-- OFFICEINFORMATION	DELETE
-- OFFICEINFORMATION	UPDATE
-- OFFICEINFORMATION	READ
select create_module_privilege_table(get_module_id('OFFICEINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OFFICEINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OFFICEINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OFFICEINFORMATION'),get_privilege_id('SEARCH'));

-- OFFICEMASTERSETUP	DELETE
-- OFFICEMASTERSETUP	UPDATE
-- OFFICEMASTERSETUP	READ
-- OFFICEMASTERSETUP	CREATE
-- OFFICEMASTERSETUP	SEARCH
select create_module_privilege_table(get_module_id('OFFICEMASTERSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OFFICEMASTERSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OFFICEMASTERSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OFFICEMASTERSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OFFICEMASTERSETUP'),get_privilege_id('SEARCH'));



-- OTHERPROCEDURES	CREATE
-- OTHERPROCEDURES	READ
-- OTHERPROCEDURES	UPDATE
select create_module_privilege_table(get_module_id('OTHERPROCEDURES'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OTHERPROCEDURES'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OTHERPROCEDURES'),get_privilege_id('UPDATE'));





-- OVERALLAUDITSTRATEGYCA	UPDATE
-- OVERALLAUDITSTRATEGYCA	READ
-- OVERALLAUDITSTRATEGYCA	CREATE
-- OVERALLAUDITSTRATEGYCA	DELETE
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYCA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYCA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYCA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYCA'),get_privilege_id('DELETE'));





-- OVERALLAUDITSTRATEGYLIST	UPDATE
-- OVERALLAUDITSTRATEGYLIST	CREATE
-- OVERALLAUDITSTRATEGYLIST	READ
-- OVERALLAUDITSTRATEGYLIST	SEARCH
-- OVERALLAUDITSTRATEGYLIST	DELETE
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYLIST'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYLIST'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYLIST'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYLIST'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('OVERALLAUDITSTRATEGYLIST'),get_privilege_id('SEARCH'));





-- PARREPORT	DELETE
-- PARREPORT	CREATE
-- PARREPORT	READ
-- PARREPORT	UPDATE
select create_module_privilege_table(get_module_id('PARREPORT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PARREPORT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PARREPORT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('PARREPORT'),get_privilege_id('DELETE'));




-- PERSONDAYSCALCULATION	CREATE
-- PERSONDAYSCALCULATION	UPDATE
-- PERSONDAYSCALCULATION	READ
select create_module_privilege_table(get_module_id('PERSONDAYSCALCULATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PERSONDAYSCALCULATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PERSONDAYSCALCULATION'),get_privilege_id('UPDATE'));



-- PRIVILEGESETUP	CREATE
-- PRIVILEGESETUP	READ
-- PRIVILEGESETUP	UPDATE
-- PRIVILEGESETUP	SEARCH
select create_module_privilege_table(get_module_id('PRIVILEGESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PRIVILEGESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PRIVILEGESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('PRIVILEGESETUP'),get_privilege_id('SEARCH'));



-- PROFESSIONALINFORMATION	UPDATE
-- PROFESSIONALINFORMATION	DELETE
-- PROFESSIONALINFORMATION	READ
-- PROFESSIONALINFORMATION	CREATE
-- PROFESSIONALINFORMATION	SEARCH
select create_module_privilege_table(get_module_id('PROFESSIONALINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PROFESSIONALINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PROFESSIONALINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('PROFESSIONALINFORMATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('PROFESSIONALINFORMATION'),get_privilege_id('SEARCH'));



-- PROFESSIONALQUALIFICATIONSETUP	READ
-- PROFESSIONALQUALIFICATIONSETUP	CREATE
-- PROFESSIONALQUALIFICATIONSETUP	SEARCH
-- PROFESSIONALQUALIFICATIONSETUP	DELETE
-- PROFESSIONALQUALIFICATIONSETUP	UPDATE
select create_module_privilege_table(get_module_id('PROFESSIONALQUALIFICATIONSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PROFESSIONALQUALIFICATIONSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PROFESSIONALQUALIFICATIONSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('PROFESSIONALQUALIFICATIONSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('PROFESSIONALQUALIFICATIONSETUP'),get_privilege_id('SEARCH'));



-- PROVINCESETUP	DELETE
-- PROVINCESETUP	READ
-- PROVINCESETUP	UPDATE
-- PROVINCESETUP	CREATE
-- PROVINCESETUP	SEARCH
select create_module_privilege_table(get_module_id('PROVINCESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('PROVINCESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('PROVINCESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('PROVINCESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('PROVINCESETUP'),get_privilege_id('SEARCH'));



-- RELEVANTCONTROLENVIRONMENT	SEARCH
-- RELEVANTCONTROLENVIRONMENT	CREATE
-- RELEVANTCONTROLENVIRONMENT	READ
-- RELEVANTCONTROLENVIRONMENT	UPDATE
-- RELEVANTCONTROLENVIRONMENT	DELETE
select create_module_privilege_table(get_module_id('RELEVANTCONTROLENVIRONMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RELEVANTCONTROLENVIRONMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RELEVANTCONTROLENVIRONMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RELEVANTCONTROLENVIRONMENT'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('RELEVANTCONTROLENVIRONMENT'),get_privilege_id('SEARCH'));



-- REVISION	UPDATE
-- REVISION	SUBMIT
-- REVISION	CREATE
-- REVISION	READ
select create_module_privilege_table(get_module_id('REVISION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('REVISION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('REVISION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('REVISION'),get_privilege_id('DELETE'));



-- RISKASSESSMENT	UPDATE
-- RISKASSESSMENT	DELETE
-- RISKASSESSMENT	SEARCH
-- RISKASSESSMENT	CREATE
-- RISKASSESSMENT	READ
select create_module_privilege_table(get_module_id('RISKASSESSMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKASSESSMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKASSESSMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKASSESSMENT'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('RISKASSESSMENT'),get_privilege_id('SEARCH'));



-- RISKDATACOLLECTION	READ
-- RISKDATACOLLECTION	UPDATE
-- RISKDATACOLLECTION	DELETE
-- RISKDATACOLLECTION	CREATE
select create_module_privilege_table(get_module_id('RISKDATACOLLECTION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKDATACOLLECTION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKDATACOLLECTION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKDATACOLLECTION'),get_privilege_id('DELETE'));




-- RISKGRADINGFORFA	UPDATE
-- RISKGRADINGFORFA	READ
-- RISKGRADINGFORFA	CREATE
-- RISKGRADINGFORFA	SEARCH
select create_module_privilege_table(get_module_id('RISKGRADINGFORFA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKGRADINGFORFA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKGRADINGFORFA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKGRADINGFORFA'),get_privilege_id('SEARCH'));




-- RISKOFFRAUD	UPDATE
-- RISKOFFRAUD	SEARCH
-- RISKOFFRAUD	DELETE
-- RISKOFFRAUD	READ
-- RISKOFFRAUD	CREATE
select create_module_privilege_table(get_module_id('RISKOFFRAUD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKOFFRAUD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKOFFRAUD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKOFFRAUD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('RISKOFFRAUD'),get_privilege_id('SEARCH'));




-- RISKOFMATERIALMISSTATEMENT	CREATE
-- RISKOFMATERIALMISSTATEMENT	DELETE
-- RISKOFMATERIALMISSTATEMENT	UPDATE
-- RISKOFMATERIALMISSTATEMENT	READ
select create_module_privilege_table(get_module_id('RISKOFMATERIALMISSTATEMENT'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKOFMATERIALMISSTATEMENT'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKOFMATERIALMISSTATEMENT'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKOFMATERIALMISSTATEMENT'),get_privilege_id('DELETE'));




-- RISKREGISTER	UPDATE
-- RISKREGISTER	CREATE
select create_module_privilege_table(get_module_id('RISKREGISTER'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKREGISTER'),get_privilege_id('UPDATE'));



-- RISKSCOREPARAMENTERFORFA	READ
-- RISKSCOREPARAMENTERFORFA	CREATE
-- RISKSCOREPARAMENTERFORFA	DELETE
-- RISKSCOREPARAMENTERFORFA	UPDATE
select create_module_privilege_table(get_module_id('RISKSCOREPARAMENTERFORFA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('RISKSCOREPARAMENTERFORFA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('RISKSCOREPARAMENTERFORFA'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('RISKSCOREPARAMENTERFORFA'),get_privilege_id('DELETE'));



-- ROLESETUP	UPDATE
-- ROLESETUP	READ
-- ROLESETUP	CONFIGURE
-- ROLESETUP	SEARCH
-- ROLESETUP	CREATE
select create_module_privilege_table(get_module_id('ROLESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('ROLESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('ROLESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('ROLESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('ROLESETUP'),get_privilege_id('SEARCH'));



-- SAMPLING	UPDATE
-- SAMPLING	READ
-- SAMPLING	CREATE
select create_module_privilege_table(get_module_id('SAMPLING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SAMPLING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SAMPLING'),get_privilege_id('UPDATE'));

-- SCREENGROUPSETUP	SEARCH
-- SCREENGROUPSETUP	CREATE
-- SCREENGROUPSETUP	READ
-- SCREENGROUPSETUP	UPDATE
select create_module_privilege_table(get_module_id('SCREENGROUPSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SCREENGROUPSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SCREENGROUPSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SCREENGROUPSETUP'),get_privilege_id('SEARCH'));


-- SCREENSETUP	UPDATE
-- SCREENSETUP	READ
-- SCREENSETUP	CREATE
-- SCREENSETUP	SEARCH
select create_module_privilege_table(get_module_id('SCREENSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SCREENSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SCREENSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SCREENSETUP'),get_privilege_id('SEARCH'));


-- SECTORALGROUP	UPDATE
-- SECTORALGROUP	READ
-- SECTORALGROUP	SEARCH
-- SECTORALGROUP	DELETE
-- SECTORALGROUP	CREATE
select create_module_privilege_table(get_module_id('SECTORALGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SECTORALGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('SECTORALGROUP'),get_privilege_id('SEARCH'));



-- SECTORALGROUPENTITYGROUP	CREATE
-- SECTORALGROUPENTITYGROUP	UPDATE
-- SECTORALGROUPENTITYGROUP	DELETE
-- SECTORALGROUPENTITYGROUP	READ
select create_module_privilege_table(get_module_id('SECTORALGROUPENTITYGROUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUPENTITYGROUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SECTORALGROUPENTITYGROUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUPENTITYGROUP'),get_privilege_id('DELETE'));


-- SECTORALGROUPOFFICE	READ
-- SECTORALGROUPOFFICE	DELETE
-- SECTORALGROUPOFFICE	CREATE
-- SECTORALGROUPOFFICE	UPDATE
select create_module_privilege_table(get_module_id('SECTORALGROUPOFFICE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUPOFFICE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SECTORALGROUPOFFICE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SECTORALGROUPOFFICE'),get_privilege_id('DELETE'));



-- SECTORCHECKLISTMAPPING	UPDATE
-- SECTORCHECKLISTMAPPING	READ
-- SECTORCHECKLISTMAPPING	CREATE
select create_module_privilege_table(get_module_id('SECTORCHECKLISTMAPPING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SECTORCHECKLISTMAPPING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SECTORCHECKLISTMAPPING'),get_privilege_id('UPDATE'));


-- SIDEBAR	READ
select create_module_privilege_table(get_module_id('SIDEBAR'),get_privilege_id('READ'));


-- SPECIALIZATIONSETUP	DELETE
-- SPECIALIZATIONSETUP	SEARCH
-- SPECIALIZATIONSETUP	CREATE
-- SPECIALIZATIONSETUP	READ
-- SPECIALIZATIONSETUP	UPDATE
select create_module_privilege_table(get_module_id('SPECIALIZATIONSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SPECIALIZATIONSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SPECIALIZATIONSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SPECIALIZATIONSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('SPECIALIZATIONSETUP'),get_privilege_id('SEARCH'));



-- SUBJECTMATTERCONSOLIDATION	READ
-- SUBJECTMATTERCONSOLIDATION	CREATE
-- SUBJECTMATTERCONSOLIDATION	SEARCH
-- SUBJECTMATTERCONSOLIDATION	UPDATE
-- SUBJECTMATTERCONSOLIDATION	DELETE
select create_module_privilege_table(get_module_id('SUBJECTMATTERCONSOLIDATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERCONSOLIDATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERCONSOLIDATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERCONSOLIDATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERCONSOLIDATION'),get_privilege_id('SEARCH'));



-- SUBJECTMATTERIDENTIFICATION	READ
-- SUBJECTMATTERIDENTIFICATION	REJECT
-- SUBJECTMATTERIDENTIFICATION	SUBMIT
-- SUBJECTMATTERIDENTIFICATION	APPROVE
-- SUBJECTMATTERIDENTIFICATION	SEARCH
-- SUBJECTMATTERIDENTIFICATION	DELETE
-- SUBJECTMATTERIDENTIFICATION	UPDATE
-- SUBJECTMATTERIDENTIFICATION	CREATE
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERIDENTIFICATION'),get_privilege_id('SEARCH'));


-- SUBJECTMATTERMASTER	REJECTDRAFT
-- SUBJECTMATTERMASTER	SUBMIT
-- SUBJECTMATTERMASTER	APPROVE
select create_module_privilege_table(get_module_id('SUBJECTMATTERMASTER'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERMASTER'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERMASTER'),get_privilege_id('APPROVE'));



-- SUBJECTMATTERSELECTION	READ
-- SUBJECTMATTERSELECTION	CREATE
-- SUBJECTMATTERSELECTION	DELETE
-- SUBJECTMATTERSELECTION	UPDATE
select create_module_privilege_table(get_module_id('SUBJECTMATTERSELECTION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERSELECTION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERSELECTION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SUBJECTMATTERSELECTION'),get_privilege_id('DELETE'));



-- SUBSTANTIVEPROCEDURE	REJECT
-- SUBSTANTIVEPROCEDURE	READ
-- SUBSTANTIVEPROCEDURE	CREATE
-- SUBSTANTIVEPROCEDURE	APPROVE
-- SUBSTANTIVEPROCEDURE	SUBMIT
-- SUBSTANTIVEPROCEDURE	DELETE
-- SUBSTANTIVEPROCEDURE	UPDATE
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('REJECT'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('SUBMIT'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('APPROVE'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SUBSTANTIVEPROCEDURE'),get_privilege_id('DELETE'));



-- SUPERVISORDASHBOARD	READ
-- SUPERVISORDASHBOARD	UPDATE
-- SUPERVISORDASHBOARD	DELETE
-- SUPERVISORDASHBOARD	CREATE
-- SUPERVISORDASHBOARD	SEARCH
select create_module_privilege_table(get_module_id('SUPERVISORDASHBOARD'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('SUPERVISORDASHBOARD'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('SUPERVISORDASHBOARD'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('SUPERVISORDASHBOARD'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('SUPERVISORDASHBOARD'),get_privilege_id('SEARCH'));



-- TASKALLOCATION	READ
-- TASKALLOCATION	CREATE
-- TASKALLOCATION	UPDATE
select create_module_privilege_table(get_module_id('TASKALLOCATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TASKALLOCATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TASKALLOCATION'),get_privilege_id('UPDATE'));

-- TEAMCOMPOSITION	READ
-- TEAMCOMPOSITION	CREATE
-- TEAMCOMPOSITION	DELETE
-- TEAMCOMPOSITION	SEARCH
-- TEAMCOMPOSITION	UPDATE
select create_module_privilege_table(get_module_id('TEAMCOMPOSITION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITION'),get_privilege_id('SEARCH'));


-- TEAMCOMPOSITIONCA	UPDATE
-- TEAMCOMPOSITIONCA	READ
-- TEAMCOMPOSITIONCA	CREATE
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONCA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONCA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONCA'),get_privilege_id('UPDATE'));



-- TEAMCOMPOSITIONMAP	UPDATE
-- TEAMCOMPOSITIONMAP	CREATE
-- TEAMCOMPOSITIONMAP	READ
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONMAP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONMAP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONMAP'),get_privilege_id('UPDATE'));


-- TEAMCOMPOSITIONOAP	UPDATE
-- TEAMCOMPOSITIONOAP	READ
-- TEAMCOMPOSITIONOAP	CREATE


select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONOAP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONOAP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONOAP'),get_privilege_id('UPDATE'));


-- TEAMCOMPOSITIONPA	CREATE
-- TEAMCOMPOSITIONPA	UPDATE
-- TEAMCOMPOSITIONPA	READ

select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONPA'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONPA'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMCOMPOSITIONPA'),get_privilege_id('UPDATE'));


-- TEAMPLANNING	READ
-- TEAMPLANNING	UPDATE
-- TEAMPLANNING	DELETE
-- TEAMPLANNING	SEARCH
-- TEAMPLANNING	CREATE
select create_module_privilege_table(get_module_id('TEAMPLANNING'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TEAMPLANNING'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TEAMPLANNING'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TEAMPLANNING'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('TEAMPLANNING'),get_privilege_id('SEARCH'));


-- TESTOFCONTROL	UPDATE
-- TESTOFCONTROL	DELETE
-- TESTOFCONTROL	READ
-- TESTOFCONTROL	CREATE
select create_module_privilege_table(get_module_id('TESTOFCONTROL'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TESTOFCONTROL'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TESTOFCONTROL'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TESTOFCONTROL'),get_privilege_id('DELETE'));


-- TESTOFCONTROLCONCLUSION	UPDATE
-- TESTOFCONTROLCONCLUSION	DELETE
-- TESTOFCONTROLCONCLUSION	CREATE
-- TESTOFCONTROLCONCLUSION	READ
select create_module_privilege_table(get_module_id('TESTOFCONTROLCONCLUSION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TESTOFCONTROLCONCLUSION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TESTOFCONTROLCONCLUSION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TESTOFCONTROLCONCLUSION'),get_privilege_id('DELETE'));


-- TESTOFCONTROLCREATE	UPDATE
-- TESTOFCONTROLCREATE	READ
-- TESTOFCONTROLCREATE	CREATE
select create_module_privilege_table(get_module_id('TESTOFCONTROLCREATE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TESTOFCONTROLCREATE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TESTOFCONTROLCREATE'),get_privilege_id('UPDATE'));


-- TRAININGINFORMATION	CREATE
-- TRAININGINFORMATION	DELETE
-- TRAININGINFORMATION	UPDATE
-- TRAININGINFORMATION	READ
-- TRAININGINFORMATION	SEARCH
select create_module_privilege_table(get_module_id('TRAININGINFORMATION'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TRAININGINFORMATION'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TRAININGINFORMATION'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TRAININGINFORMATION'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('TRAININGINFORMATION'),get_privilege_id('SEARCH'));


-- TRAININGTYPESETUP	CREATE
-- TRAININGTYPESETUP	SEARCH
-- TRAININGTYPESETUP	DELETE
-- TRAININGTYPESETUP	UPDATE
-- TRAININGTYPESETUP	READ
select create_module_privilege_table(get_module_id('TRAININGTYPESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('TRAININGTYPESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('TRAININGTYPESETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('TRAININGTYPESETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('TRAININGTYPESETUP'),get_privilege_id('SEARCH'));


-- UNSETTLEMENTREASONSETUP	UPDATE
-- UNSETTLEMENTREASONSETUP	CREATE
-- UNSETTLEMENTREASONSETUP	READ
-- UNSETTLEMENTREASONSETUP	SEARCH
-- UNSETTLEMENTREASONSETUP	DELETE
select create_module_privilege_table(get_module_id('UNSETTLEMENTREASONSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('UNSETTLEMENTREASONSETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('UNSETTLEMENTREASONSETUP'),get_privilege_id('UPDATE'));
select create_module_privilege_table(get_module_id('UNSETTLEMENTREASONSETUP'),get_privilege_id('DELETE'));
select create_module_privilege_table(get_module_id('UNSETTLEMENTREASONSETUP'),get_privilege_id('SEARCH'));

-- UPDATECHECKLIST	CREATE
select create_module_privilege_table(get_module_id('UPDATECHECKLIST'),get_privilege_id('CREATE'));

-- USERPRIVILEGESETUP	READ
-- USERPRIVILEGESETUP	UPDATE
-- USERPRIVILEGESETUP	CREATE
select create_module_privilege_table(get_module_id('USERPRIVILEGESETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('USERPRIVILEGESETUP'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('USERPRIVILEGESETUP'),get_privilege_id('UPDATE'));


-- USERSETUP	READ
-- USERSETUP	CREATE
select create_module_privilege_table(get_module_id('USERSETUP'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('USERSETUP'),get_privilege_id('READ'));


-- WRITTENREPRESENTATIONCONCLUSION	READ
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONCONCLUSION'),get_privilege_id('READ'));


-- WRITTENREPRESENTATIONENTITY	UPDATE
-- WRITTENREPRESENTATIONENTITY	READ
-- WRITTENREPRESENTATIONENTITY	CREATE

select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONENTITY'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONENTITY'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONENTITY'),get_privilege_id('UPDATE'));

-- WRITTENREPRESENTATIONOFFICE	UPDATE
-- WRITTENREPRESENTATIONOFFICE	CREATE
-- WRITTENREPRESENTATIONOFFICE	READ
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONOFFICE'),get_privilege_id('CREATE'));
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONOFFICE'),get_privilege_id('READ'));
select create_module_privilege_table(get_module_id('WRITTENREPRESENTATIONOFFICE'),get_privilege_id('UPDATE'));






